/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package identityChange
	
import (
	
	A	"git.duniter.org/gerard94/util/avlT"
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	ST	"git.duniter.org/gerard94/util/strings"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"bufio"
		"net/http"
		"strconv"
		"strings"
		"html/template"
		
		/*
		"fmt"
		*/

)

const (
	
	identityChangeName = "43identityChange"
	
	queryFindIdentity = `
		query FindIdentity ($nN: String!) {
			id:identityOf(nickname: $nN) {
				nickname
				password
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	queryListLocations = `
		query ListLocations ($co: String) {
			locList: allLocations(country: $co) {
				country
				regions
			}
		}
	`
	
	queryListServices = `
		query ListServices ($ca: String) {
			serList: allServices(category: $ca) {
				category
				specialities
			}
		}
	`
	
	queryChangeIdentity = `
		mutation ChangeIdentity ($nN: String!, $id: Identity_input!) {
			Id:changeIdentity(nickname: $nN, newId: $id) {
				nickname
				password
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{if .Fix1}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
				{{end}}
				{{with .Fix1}}
					<p>
						{{.Nickname}}
					</p>
					<p>
						<label for="g1nId">{{.G1nLbl}}</label>
						<input type="text" name="g1n" id="g1nId" placeholder="{{.G1nPH}}" value="{{.G1_nickname}}" size = 35/>
					</p>
					<p>
						<label for="pubId">{{.PubLbl}}</label>
						<input type="text" name="pub" id="pubId" placeholder="{{.PubPH}}" value="{{.Pubkey}}" size = 35/>
					</p>
					<p>
						<label for="mailId">{{.MailLbl}}</label>
						<input type="text" name="mail" id="mailId" placeholder="{{.MailPH}}" value="{{.Email}}" size = 35/>
					</p>
				{{end}}
				{{with .FixLoc}}
					<p>
						<label for="locsId">{{.LocsLbl}}</label>
						{{$lPos := .LocsPos}}
						<select name="locations" id="locsId" multiple>
							{{range $co := .Locs}}
								{{range $rg := $co.Regions}}
									<option value="{{$co.Country}}|{{$rg}}" {{range $l := $lPos}} {{if and (eq $co.Country $l.Country) (eq $rg $l.Region) }}selected{{end}}{{end}}>{{$co.Country}} / {{$rg}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
					<input type="hidden" name="locsPosH" value="{{.LocsPosH}}"/>
				{{end}}
				{{with .FixCities}}
					{{$names := .Names}}
					{{$nameIds := .NameIds}}
					{{$cities := .Cities}}
					{{range $ci, $cl := .CyLbl}}
						<p>
							<label for="{{index $nameIds $ci}}">{{$cl}} </label>
							<br>
							<textarea id="{{index $nameIds $ci}}" name="{{index $names $ci}}" rows=5 cols=50>{{index $cities $ci}}</textarea>
						</p>
						<br>
					{{end}}
				{{end}}
				{{with .Fix2}}
					<p>
						<label for="tranId">{{.TranLbl}}</label>
						<input type="checkbox" name="tran" id="tranId" {{if .Transport}}checked{{end}}/>
					</p>
					<p>
						<label for="pickId">{{.PickLbl}}</label>
						<input type="checkbox" name="pick" id="pickId" {{if .Pickup}}checked{{end}}/>
					</p>
				{{end}}
				{{with .FixSu}}
					<p>
						<label for="su_sersId">{{.SersLbl}}</label>
						{{$suPos := .SersPos}}
						<select name="su_sers" id="su_sersId" multiple>
							{{range $ca := .Sers}}
								{{range $sp := $ca.Specialities}}
									<option value="{{$ca.Category}}|{{$sp}}" {{range $s := $suPos}} {{if and (eq $ca.Category $s.Category) (eq $sp $s.Speciality) }}selected{{end}}{{end}}>{{$ca.Category}} / {{$sp}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
				{{end}}
				{{with .FixSe}}
					<p>
						<label for="se_sersId">{{.SersLbl}}</label>
						{{$sePos := .SersPos}}
						<select name="se_sers" id="se_sersId" multiple>
							{{range $ca := .Sers}}
								{{range $sp := $ca.Specialities}}
									<option value="{{$ca.Category}}|{{$sp}}" {{range $s := $sePos}} {{if and (eq $ca.Category $s.Category) (eq $sp $s.Speciality) }}selected{{end}}{{end}}>{{$ca.Category}} / {{$sp}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
				{{end}}
				{{with .Fix2}}
					<p>
						<label for="addiId">{{.AddInfosLbl}}</label>
						<input type="text" name="addi" id="addiId" placeholder="{{.AddInfosPH}}" value="{{.AddInfos}}" size = 35/>
					</p>
				{{end}}
				{{if .Message}}
					<p>
					 {{.Message}}
					</p>
					<input type="hidden" name="message" value="done"/>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Location1Short struct {
		Country,
		Region string
	}
	
	LocationShort struct {
		Country string
		Regions []string
	}
	
	Region struct {
		Region string
		Cities []string
	}
	
	Location struct {
		Country string
		Regions []Region
	}
	
	Service1 struct {
		Category,
		Speciality string
	}
	
	Service struct {
		Category string
		Specialities []string
	}
	
	Identity struct {
		Nickname,
		Password,
		G1_nickname,
		Pubkey,
		Email string
		Locations []Location
		Transport,
		Pickup bool
		Supplied_services,
		Searched_services []Service
		Additional_informations string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			IdList []*Identity
			Id *Identity
			LocList []LocationShort
			SerList []Service
		}
		Id *Identity
	}
	
	// Outputs
	
	LocList struct {
	}
	
	SerList struct {
	}
	
	Fix1 struct {
		Nickname,
		G1nLbl,
		G1nPH,
		G1_nickname,
		PubLbl,
		PubPH,
		Pubkey,
		MailLbl,
		MailPH,
		Email string
	}
	
	Fix2 struct {
		TranLbl,
		PickLbl string
		Transport,
		Pickup bool
		AddInfosLbl,
		AddInfosPH,
		AddInfos string
	}
	
	FixLoc struct {
		LocsLbl,
		LocsPosH string
		LocsPos []Location1Short
		Locs []LocationShort
	}
		
	FixCities struct {
		CyLbl,
		Names,
		NameIds,
		Cities []string
	}
	
	FixSers struct {
		SersLbl string
		SersPos []Service1
		Sers []Service
	}
	
	Out struct {
		Error,
		Message,
		Title,
		OK string
		Fix1 *Fix1
		FixLoc *FixLoc
		FixCities *FixCities
		Fix2 *Fix2
		FixSu,
		FixSe *FixSers
	}

)

var (
	
	idFindDoc = GS.ExtractDocument(queryFindIdentity)
	locListDoc = GS.ExtractDocument(queryListLocations)
	serListDoc = GS.ExtractDocument(queryListServices)
	idChgDoc = GS.ExtractDocument(queryChangeIdentity)

)

func (loc1 *LocationShort) Compare (loc2 *LocationShort) ST.Comp {
	return ST.CompP(loc1.Country, loc2.Country)
}

func (ser1 *Service) Compare (ser2 *Service) ST.Comp {
	return ST.CompP(ser1.Category, ser2.Category)
}

func doBase (t string, errs []BA.Error, lang *SM.Lang) (title, error, ok string) {
	title = lang.Map(t)
	error = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func locsPosToString (locsPos []Location1Short) string {
	var b strings.Builder
	for i, pos := range locsPos {
		if i > 0 {
			b.WriteString("|")
		}
		b.WriteString(pos.Country)
		b.WriteString("|")
		b.WriteString(pos.Region)
	}
	return b.String()
}

func stringToLocsPos (s string) []Location1Short {
	l := strings.Split(s, "|")
	locsPos := make([]Location1Short , len(l) / 2)
	for i, _ := range locsPos {
		locsPos[i] = Location1Short{l[2 * i], l[2 * i + 1]}
	}
	return locsPos
}

func doFixLoc (locsPos []Location1Short, locs []LocationShort, lang *SM.Lang) *FixLoc {
	lLbl := lang.Map("#skillsclient:Locations", "*") + BA.SpL
	lph := locsPosToString(locsPos)
	return &FixLoc{lLbl, lph, locsPos, locs}
}

func transferCities (oldLocsPos, locsPos []Location1Short, oldCities []string) []string {
	cities := make([]string, len(locsPos))
	for i, loc := range locsPos {
		for j, oLoc := range oldLocsPos {
			if loc == oLoc {
				cities[i] = oldCities[j]
				break
			}
		}
	}
	return cities
}

func doFixCities (lp []Location1Short, cities []string, lang *SM.Lang) *FixCities {
	n := len(lp)
	cl := make([]string, n)
	names := make([]string, n)
	nameIds := make([]string, n)
	for i, p := range lp {
		cl[i] = lang.Map("#skillsclient:TypeCities", p.Country, p.Region)
		a := strconv.Itoa(i)
		names[i] = "cities" + a
		nameIds[i] = "citiesId" + a
	}
	return &FixCities{cl, names, nameIds, cities}
}

func doFixSu (sersPos []Service1, sers []Service, lang *SM.Lang) *FixSers {
	sLbl := lang.Map("#skillsclient:Supplied_services", "*") + BA.SpL
	return &FixSers{sLbl, sersPos, sers}
}

func doFixSe (sersPos []Service1, sers []Service, lang *SM.Lang) *FixSers {
	sLbl := lang.Map("#skillsclient:Searched_services") + BA.SpL
	return &FixSers{sLbl, sersPos, sers}
}

func doFix1 (id *Identity, lang *SM.Lang) *Fix1 {
	nN := lang.Map("#skillsclient:NicknameL", "") + BA.SpL + id.Nickname
	gNNLbl := lang.Map("#skillsclient:G1_nickname") + BA.SpL
	gNNPH := lang.Map("#skillsclient:StringPlaceHolder")
	gNN := id.G1_nickname
	pubLbl := lang.Map("#skillsclient:Pubkey", "*") + BA.SpL
	pubPH := lang.Map("#skillsclient:StringPlaceHolder")
	pub := id.Pubkey
	mailLbl := lang.Map("#skillsclient:Email") + BA.SpL
	mailPH := lang.Map("#skillsclient:StringPlaceHolder")
	mail := id.Email
	return &Fix1{nN, gNNLbl, gNNPH, gNN, pubLbl, pubPH, pub, mailLbl, mailPH, mail}
}

func doFix2 (id *Identity, lang *SM.Lang) *Fix2 {
	M.Assert(id != nil, 20)
	tranLbl := lang.Map("#skillsclient:Transport") + BA.SpL
	transport := id.Transport
	pickLbl := lang.Map("#skillsclient:Pickup") + BA.SpL
	pickup := id.Pickup
	addLbl := lang.Map("#skillsclient:Add_Info") + BA.SpL
	addPH := lang.Map("#skillsclient:StringPlaceHolder")
	add := id.Additional_informations
	return &Fix2{tranLbl, pickLbl, transport, pickup, addLbl, addPH, add}
}

func printFix1 (t string, id *Identity, locsPos []Location1Short, locs []LocationShort, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	fl := doFixLoc(locsPos, locs, lang)
	fix1 := doFix1(id, lang)
	return &Out{Error: err, Title: t, OK: ok, Fix1: fix1, FixLoc: fl}
}

func locToLoc1ShortCities (locs []Location) ([]Location1Short, []string) {
	l1s := make([]Location1Short, 0)
	cities := make([]string, 0)
	for _, co := range locs {
		for _, rg := range co.Regions {
			l1s = append(l1s, Location1Short{co.Country, rg.Region})
			b := new(strings.Builder)
			for i, cy := range rg.Cities {
				if i > 0 {
					b.WriteString("\n")
				}
				b.WriteString(cy)
			}
			cities = append(cities, b.String())
		}
	}
	return l1s, cities
}

func serToSer1 (sers []Service) []Service1 {
	s1 := make([]Service1, 0)
	for _, ca := range sers {
		for _, sp := range ca.Specialities {
			s1 = append(s1, Service1{ca.Category, sp})
		}
	}
	return s1
}

func printFix2 (t string, id *Identity, locsPos []Location1Short, cities []string, locs []LocationShort, suPos, sePos []Service1, sers []Service, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	fl := doFixLoc(locsPos, locs, lang)
	fc := doFixCities(locsPos, cities, lang)
	fix1 := doFix1(id, lang)
	fix2 := doFix2(id, lang)
	fixsu := doFixSu(suPos, sers, lang)
	fixse := doFixSe(sePos, sers, lang)
	return &Out{Error: err, Title: t, OK: ok, Fix1: fix1, FixLoc: fl, FixCities: fc, Fix2: fix2, FixSu: fixsu, FixSe: fixse}
}

func buildJson1 (mk *J.Maker, nick, pass, g1n, pub, mail string) {
	mk.PushString(nick)
	mk.BuildField("nickname")
	mk.PushString(pass)
	mk.BuildField("password")
	mk.PushString(g1n)
	mk.BuildField("g1_nickname")
	mk.PushString(pub)
	mk.BuildField("pubkey")
	mk.PushString(mail)
	mk.BuildField("email")
}

func buildJson2 (mk *J.Maker, l *A.Tree[*LocationShort], citiesList [][]string) {
	mk.StartArray()
	i := 0
	for e := l.Next(nil); e != nil; e = l.Next(e) {
		loc := e.Val()
		mk.StartObject()
		mk.PushString(loc.Country)
		mk.BuildField("country")
		mk.StartArray()
		for _, rg := range loc.Regions {
			mk.StartObject()
			mk.PushString(rg)
			mk.BuildField("region")
			mk.StartArray()
			if citiesList != nil {
				for _, cy := range citiesList[i] {
					mk.PushString(cy)
				}
			}
			mk.BuildArray()
			mk.BuildField("cities")
			mk.BuildObject()
			i++
		}
		mk.BuildArray()
		mk.BuildField("regions")
		mk.BuildObject()
	}
	mk.BuildArray()
	mk.BuildField("locations")
}

func buildJsonSer (mk *J.Maker, serPos *A.Tree[*Service]) {
	mk.StartArray()
	for e := serPos.Next(nil); e != nil; e = serPos.Next(e) {
		ser := e.Val()
		mk.StartObject()
		mk.PushString(ser.Category)
		mk.BuildField("category")
		mk.StartArray()
		for _, sp := range ser.Specialities {
			mk.PushString(sp)
		}
		mk.BuildArray()
		mk.BuildField("specialities")
		mk.BuildObject()
	}
	mk.BuildArray()
}

func buildJson3 (mk *J.Maker, tran, pick, addi string, suPos, sePos *A.Tree[*Service]) {
	mk.PushBoolean(tran != "")
	mk.BuildField("transport")
	mk.PushBoolean(pick != "")
	mk.BuildField("pickup")
	buildJsonSer(mk, suPos)
	mk.BuildField("supplied_services")
	buildJsonSer(mk, sePos)
	mk.BuildField("searched_services")
	mk.PushString(addi)
	mk.BuildField("additional_informations")
}

func textsToStrings (texts []string) ([]string, [][]string) {
	t := make([]string, len(texts))
	ss := make([][]string, len(texts))
	for i, text := range texts {
		s := make([]string, 0)
		if text != "" {
			sc := bufio.NewScanner(strings.NewReader(text))
			for sc.Scan() {
				s = append(s, BA.CleanText(sc.Text()))
			}
		}
		for j := 1; j < len(s); j++ {
			for k := j; k > 0 && ST.CompP(s[k], s[k - 1]) == ST.Lt; k-- {
				s[k], s[k - 1] = s[k - 1], s[k]
			}
		}
		ss[i] = s
		b := new(strings.Builder)
		for j, sj := range s {
			if j > 0 {
				b.WriteString("\n")
			}
			b.WriteString(sj)
		}
		t[i] = b.String()
	}
	return t, ss
} //textsToStrings

func loc1ToLoc (l1 []Location1Short) *A.Tree[*LocationShort] {
	t := A.New[*LocationShort]()
	for _, ll := range l1 {
		e, _, _ := A.SearchIns[*LocationShort](t, &LocationShort{ll.Country, make([]string, 0)})
		l := e.Val()
		l.Regions = append(l.Regions, ll.Region)
	}
	for e := t.Next(nil); e != nil; e = t.Next(e) {
		l := e.Val()
		rgs := l.Regions
		for i := 1; i < len(rgs); i++ {
			for j := i; j > 0 && ST.CompP(rgs[j], rgs[j - 1]) == ST.Lt; j-- {
				rgs[j], rgs[j - 1] = rgs[j - 1], rgs[j]
			}
		}
		l.Regions = rgs
		e.SetVal(l)
	}
	return t
}

func servicesPos (sers []string) []Service1 {
	ss := make([]Service1, len(sers))
	for i, s := range sers {
		l := strings.Split(s, "|")
		ss[i] = Service1{l[0], l[1]}
	}
	return ss
}

func ser1ToSer (s1 []Service1) *A.Tree[*Service] {
	l := A.New[*Service]()
	for _, ss := range s1 {
		e, _, _ := A.SearchIns[*Service](l, &Service{ss.Category, make([]string, 0)})
		s := e.Val()
		s.Specialities = append(s.Specialities, ss.Speciality)
	}
	for e := l.Next(nil); e != nil; e = l.Next(e) {
		s := e.Val()
		sps := s.Specialities
		for i := 1; i < len(sps); i++ {
			for j := i; j > 0 && ST.CompP(sps[j], sps[j - 1]) == ST.Lt; j-- {
				sps[j], sps[j - 1] = sps[j - 1], sps[j]
			}
		}
		s.Specialities = sps
		e.SetVal(s)
	}
	return l
}

func cleanPass (text string) string {
	return strings.TrimSpace(text)
} //cleanPass

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == identityChangeName, name, 100)
	t := "#skillsclient:IdentityChange"
	
	isNew := r.Method == "GET"
	
	r.ParseForm()
	mes := r.PostFormValue("message")
	if mes != "" {
		http.Redirect(w, r, "/index", http.StatusFound)
		return
	}
	
	j := GS.Send(nil, locListDoc, "")
	resLoc := new(Res)
	J.ApplyTo(j, resLoc)
	M.Assert(resLoc.Errors == nil, 101)
	
	j = GS.Send(nil, serListDoc, "")
	resSer := new(Res)
	J.ApplyTo(j, resSer)
	M.Assert(resSer.Errors == nil, 104)
	
	nick, _, _ := CB.VerifyToken(data.Token())
	
	var (locsPos []Location1Short; cities [] string; suPos, sePos []Service1)
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(nick)
	mk.BuildField("nN")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, idFindDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	
	pass := res.Data.Id.Password
	
	if isNew {
			
		locsPos, cities = locToLoc1ShortCities(res.Data.Id.Locations)
		
		suPos = serToSer1(res.Data.Id.Supplied_services)
		sePos = serToSer1(res.Data.Id.Searched_services)
		
	} else {
		
		g1n := r.PostFormValue("g1n")
		pub := r.PostFormValue("pub")
		mail := r.PostFormValue("mail")
		locations := r.PostForm["locations"]
		locsPos = make([]Location1Short, len(locations))
		for i, loc := range locations {
			l := strings.Split(loc, "|")
			locsPos[i] = Location1Short{l[0], l[1]}
		}
		
		if len(locsPos) == 0 || pub == "" {
			mk := J.NewMaker()
			mk.StartObject()
			mk.StartObject()
			buildJson1(mk, nick, pass, g1n, pub, mail)
			buildJson2(mk, A.New[*LocationShort](), nil)
			mk.BuildObject()
			mk.BuildField("id")
			mk.BuildObject()
			j = mk.GetJson()
			res = new(Res)
			J.ApplyTo(j, res)
			out := printFix1(t, res.Id, locsPos, resLoc.Data.LocList, nil, lang)
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 103)
			return
		}
		
		locsPosH := stringToLocsPos(r.PostFormValue("locsPosH"))
		cities = make([]string, len(locsPosH))
		for i := 0; i < len(locsPosH); i++ {
			cities[i] = BA.CleanText(r.PostFormValue("cities" + strconv.Itoa(i)))
		}
		cities = transferCities(locsPosH, locsPos, cities)
		
		tran := r.PostFormValue("tran")
		pick := r.PostFormValue("pick")
		
		su := r.PostForm["su_sers"]
		suPos = servicesPos(su)
		se := r.PostForm["se_sers"]
		sePos = servicesPos(se)
		
		addi := r.PostFormValue("addi")
		
		l := loc1ToLoc(locsPos)
		
		ok := len(suPos) != 0 && len(locsPos) == len(locsPosH)
		if ok {
			 for i := 0; i < len(locsPos); i++ {
				if locsPos[i] != locsPosH[i] {
					ok = false
					break
				}
			}
		}
		cities, citiesList := textsToStrings(cities)
		if !ok {
			mk := J.NewMaker()
			mk.StartObject()
			mk.PushString(nick)
			mk.BuildField("nN")
			mk.StartObject()
			buildJson1(mk, nick, pass, g1n, pub, mail)
			buildJson2(mk, l, citiesList)
			buildJson3(mk, tran, pick, addi, ser1ToSer(suPos), ser1ToSer(sePos))
			mk.BuildObject()
			mk.BuildField("id")
			mk.BuildObject()
			j = mk.GetJson()
			res = new(Res)
			J.ApplyTo(j, res)
			out := printFix2(t, res.Id, locsPos, cities, resLoc.Data.LocList, suPos, sePos, resSer.Data.SerList, nil, lang)
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 105)
			return
		}
		mk = J.NewMaker()
		mk.StartObject()
		mk.PushString(nick)
		mk.BuildField("nN")
		mk.StartObject()
		buildJson1(mk, nick, pass, g1n, pub, mail)
		buildJson2(mk, l, citiesList)
		buildJson3(mk, tran, pick, addi, ser1ToSer(suPos), ser1ToSer(sePos))
		mk.BuildObject()
		mk.BuildField("id")
		mk.BuildObject()
		j = mk.GetJson()
		j = GS.Send(j, idChgDoc, "")
		res = new(Res)
		J.ApplyTo(j, res)
		if res.Errors == nil {
			out := &Out{Message: lang.Map("#skillsclient:Recorded"), OK: lang.Map("#skillsclient:OK")}
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 106)
			return
		}
		if res.Data.Id == nil {
			res.Data.Id = new(Identity)
		}
	}
	
	out := printFix2(t, res.Data.Id, locsPos, cities, resLoc.Data.LocList, suPos, sePos, resSer.Data.SerList, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 106)
} //end

func init() {
	W.RegisterPackage(identityChangeName, html, end, true)
} //init
