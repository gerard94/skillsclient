/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package basic

import (
	
	A	"git.duniter.org/gerard94/util/alea"
	F	"path/filepath"
	G	"git.duniter.org/gerard94/util/graphQL"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
	SC	"strconv"
	SM	"git.duniter.org/gerard94/util/strMapping"
		"bufio"
		"errors"
		"fmt"
		"os"
		"strings"
		"text/scanner"
		"unicode"

)

const (
	
	rsrcDir = "skillsclient"
	
	serverDefaultAddress = "http://localhost:8081"
	serverAddressName = "serverAddress.txt"
	
	htmlDefaultAddress = "localhost:7071"
	htmlAddressName = "htmlAddress.txt"
	
	skillsclientName = "skillsclient"
	adminsName = "skillsclient/admins.txt"
	
	passLength = 12
	
	SpS = "\u00A0" // Short space
	SpL = SpS + SpS + SpS + SpS // Long space

)

type (
	
	Loc struct {
		Position,
		Line,
		Column int
	}
	
	Error struct {
		Message string
		Locations []Loc
		Path []any
	}

)

var (
	
	rsrcPath = R.FindDir(rsrcDir)
	serverAddress = serverDefaultAddress
	htmlAddress = htmlDefaultAddress
	
	skillsclientPath = R.FindDir(skillsclientName)
	adminsPath = R.FindDir(adminsName)
	
	admins []string

)

func ServerAddress () string {
	return serverAddress
}

func HtmlAddress () string {
	return htmlAddress
}

func CleanText (text string) string {
		
	isNotGoodChar := func (r rune) bool {
		return !(unicode.IsLetter(r) || '0' <= r && r <= '9' || r == '-' || r == '_' || r == 13 || r == 10)
	} //isNotGoodChar
	
	//CleanText
	return strings.TrimFunc(text, isNotGoodChar)
} //CleanText

func readAdmins () {
	f, err := os.Open(adminsPath)
	if err == nil {
		defer f.Close()
		sc := bufio.NewScanner(f)
		admins = make([]string, 0)
		for sc.Scan() {
			t := CleanText(sc.Text())
			if t != "" {
				admins = append(admins, t)
			}
		}
	} else {
		f, err := os.Create(adminsPath)
		M.Assert(err == nil, err, 100)
		defer f.Close()
		admins = make([]string, 0)
	}
}

func Admins () []string {
	return admins
}

func IsAdmin (nick string) bool {
	for _, n := range admins {
		if n == nick {
			return true
		}
	}
	return false
}

func Password () string {
	var b strings.Builder
	for i := 0; i < passLength; i++ {
		b.WriteRune(rune(A.IntRand('a', 'z' + 1)))
	}
	return b.String()
}

func DoError (errs []Error, lang *SM.Lang) string {
	if errs == nil {
		return ""
	}
	var b strings.Builder
	i := 0
	for _, err := range errs {		
		switch {
		case err.Message[0] == '|':
			l := strings.Split(err.Message, "|")
			n := len(l); M.Assert(n >= 3, n, 100)
			base := l[1]
			args := l[2:n-1]
			return lang.Map("#skillsclient:" + base, args...)
		case err.Locations != nil && err.Locations[0].Position < 0:
		default:
			if i > 0 {
				b.WriteString("; ")
			}
			i++
			b.WriteString(err.Message)
			if err.Locations != nil {
				b.WriteString(" at")
				for j, loc := range err.Locations {
					if j > 0 {
						b.WriteString(",")
					}
					b.WriteString(" ")
					b.WriteString(SC.Itoa(loc.Line))
					b.WriteString(":")
					b.WriteString(SC.Itoa(loc.Column))
				}
			}
			path := err.Path
			if path != nil {
				b.WriteString(" in ")
				for j, p := range path {
					if j > 0 {
						b.WriteString(".")
					}
					switch p := p.(type) {
					case *G.PathString:
						b.WriteString(p.S)
					case *G.PathNb:
						b.WriteString(SC.Itoa(p.N))
					}
				}
			}
		}
	}
	return b.String()
} //DoError

func fixAddress (adrName string, adr *string) {
	name := F.Join(rsrcPath, adrName)
	f, err := os.Open(name)
	if err == nil {
		defer f.Close()
		s := new(scanner.Scanner)
		s.Init(f)
		s.Error = func(s *scanner.Scanner, msg string) {M.Halt(errors.New("File " + name + " incorrect"), 100)}
		s.Mode = scanner.ScanStrings
		s.Scan()
		ss := s.TokenText()
		M.Assert(ss[0] == '"' && ss[len(ss) - 1] == '"', ss, 101)
		*adr = ss[1:len(ss) - 1]
	} else {
		f, err := os.Create(name)
		M.Assert(err == nil, err, 102)
		defer f.Close()
		fmt.Fprint(f, "\"" + *adr + "\"")
	}
} // fixAddress

func init () {
	err := os.MkdirAll(skillsclientPath, 0777); M.Assert(err == nil, err, 100)
	fixAddress(serverAddressName, &serverAddress)
	fixAddress(htmlAddressName, &htmlAddress)
	readAdmins()
}
