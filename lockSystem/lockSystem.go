/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package lockSystem
	
import (
	
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"sync"
		"html/template"
		"time"

)

const (
	
	lockSystemName = "34lockSystem"
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Message}}
				{{.Message}}
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`
	
	tf = "2006/01/02 15:04:05"

)

type (
	
	// Outputs
	
	Out struct {
		Title,
		Message,
		OK string
	}

)

var (
	
	systemMut sync.Mutex

)

func printInit (t string, lang *SM.Lang) *Out {
	title := lang.Map(t)
	var ok string
	if W.IsOnlyUser() {
		ok = lang.Map("#skillsclient:UnlockSystem")
	} else {
		ok = lang.Map("#skillsclient:LockSystem")
	}
	return &Out{Title: title, OK: ok}
}

func printLocked (t, nick string, lang *SM.Lang) *Out {
	title := lang.Map(t)
	mes := lang.Map("#skillsclient:SystemLocked", nick, time.Now().Add(W.BlockingMaxDuration).Format(tf))
	ok := lang.Map("#skillsclient:UnlockSystem")
	return &Out{Title: title, Message: mes, OK: ok}
}

func printUnlocked (t string, lang *SM.Lang) *Out {
	title := lang.Map(t)
	mes := lang.Map("#skillsclient:SystemUnlocked")
	ok := lang.Map("#skillsclient:LockSystem")
	return &Out{Title: title, Message: mes, OK: ok}
}

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	t := "#skillsclient:LockSystem"
	if r.Method == "GET" {
		out := printInit(t, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 100)
		return
	}
	var out *Out
	systemMut.Lock()
	if !W.IsOnlyUser() {
		nick, _, err := CB.VerifyToken(data.Token()); M.Assert(err == nil, err, 101)
		W.SetOnlyUser(nick)
		out = printLocked(t, nick, lang)
	} else {
		W.UnsetOnlyUser()
		out = printUnlocked(t, lang)
	}
	systemMut.Unlock()
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //end

func init() {
	W.RegisterPackage(lockSystemName, html, end, true)
} //init
