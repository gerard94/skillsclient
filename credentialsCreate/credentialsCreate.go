/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package credentialsCreate
	
import (
	
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	G	"git.duniter.org/gerard94/util/graphQL"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"bufio"
		"net/http"
		"strings"
		"html/template"
		
		/*
		"fmt"
		*/

)

const (
	
	credentialsCreateName = "311credentialsCreate"
	credentialsResetName = "312credentialsReset"
	
	queryCreateCredentials = `
		mutation CreateCredentials ($ids: [Identity_input!]!) {
			ids:insertIdentities(ids: $ids) {
				nickname
			}
		}
	`
	
	queryIdentityOf = `
		query IdentityOf ($nN: String!) {
			id:identityOf(nickname: $nN) {
				nickname
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	queryResetCredentials = `
		mutation ResetCredentials ($ids: [Identity_input!]!) {
			ids:changeIdentities(ids: $ids) {
				nickname
			}
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .Fix}}
					<p>
						<label for="nicksId">{{.NicksLbl}}</label>
						<br>
						<textarea name="nicks" id="nicksId" rows=20 cols=50>{{.NickNames}}</textarea>
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			{{with .Answer}}
				{{$nick := .NickLbl}}
				{{$pass := .PassLbl}}
				{{$sep := .SepLbl}}
				<p>
				{{range $id := .Credentials}}
					{{with $id}}
						<br>
						{{$nick}} {{$id.Nickname}} {{$sep}} {{$pass}} {{$id.Password}}
					{{end}}
				{{end}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

const (
	
	passLength = 12

)

type (
	
	Identity struct {
		Nickname,
		Password string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			Ids []*Identity
		}
	}
	
	// Outputs
	
	Fix struct {
		NicksLbl,
		NickNames string
	}
	
	Answer struct {
		NickLbl,
		PassLbl,
		SepLbl string
		Credentials []*Identity
	}
	
	Out struct {
		Error,
		Title,
		OK string
		Fix *Fix
		Answer *Answer
	}

)

var (
	
	credCDoc = GS.ExtractDocument(queryCreateCredentials)
	idDoc = GS.ExtractDocument(queryIdentityOf)
	credRDoc = GS.ExtractDocument(queryResetCredentials)

)

func doBase (t string, errs []BA.Error, lang *SM.Lang) (title, error, ok string) {
	title = lang.Map(t)
	error = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doFix (nicknames string, lang *SM.Lang) *Fix {
	nNLbl := lang.Map("#skillsclient:Nicknames", "*") + BA.SpL
	return &Fix{nNLbl, nicknames}
}

func printFix (t, nicknames string, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	fix := doFix(nicknames, lang)
	return &Out{Error: err, Title: t, OK: ok, Fix: fix}
}

func doAnswer (cred []*Identity, lang *SM.Lang) *Answer {
	nL := lang.Map("#skillsclient:NicknameL", "") + BA.SpS
	pL := lang.Map("#skillsclient:PassLbl", "") + BA.SpS
	sep := BA.SpL
	return &Answer{nL, pL, sep, cred}
}

func printAnswer (t string, cred []*Identity, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	ans := doAnswer(cred, lang)
	return &Out{Error: err, Title: t, OK: ok, Answer: ans}
}

func cleanText (text string) string {
	
	IsNotUidChar := func (r rune) bool {
		return !('a' <= r  && r <= 'z' || 'A' <= r && r <= 'Z' || '0' <= r && r <= '9' || r == '-' || r == '_' || r == 13 || r == 10)
	} //IsNotUidChar
	
	//cleanText
	return strings.TrimFunc(text, IsNotUidChar)
} //cleanText

func textToStringList (text string) []string {
	sc := bufio.NewScanner(strings.NewReader(text))
	l := make([]string, 0)
	for sc.Scan() {
		l = append(l, cleanText(sc.Text()))
	}
	return l
} //textToStringList

func stringListToText (l []string) string {
	var b strings.Builder
	for i, s := range l {
		if i > 0 {
			b.WriteString("\n")
		}
		b.WriteString(s)
	}
	return b.String()
} //stringListToText

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	var (doc *G.Document; t string)
	switch name {
	case credentialsCreateName:
		doc = credCDoc
		t = "#skillsclient:CredentialsCreate"
	case credentialsResetName:
		doc = credRDoc
		t = "#skillsclient:CredentialsReset"
	default:
		M.Halt(name, 100)
	}
	
	if r.Method == "GET" {
		out := printFix(t, "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	
	nicks := cleanText(r.PostFormValue("nicks"))
	
	if nicks == "" {
		out := printFix(t, "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
		return
	}
	
	nicksList := textToStringList(nicks)
	nicks = stringListToText(nicksList)
	
	credentials := make([]*Identity, len(nicksList))
	mk := J.NewMaker()
	mk.StartObject()
	mk.StartArray()
	if name == credentialsCreateName {
		for i, nick := range nicksList {
			mk.StartObject()
			mk.PushString(nick)
			mk.BuildField("nickname")
			pass := BA.Password()
			mk.PushString(CB.CryptPass(pass))
			mk.BuildField("password")
			mk.BuildObject()
			credentials[i] = &Identity{Nickname: nick, Password: pass}
		}
	} else {
		for i, nick := range nicksList {
			m := J.NewMaker()
			m.StartObject()
			m.PushString(nick)
			m.BuildField("nN")
			m.BuildObject()
			j := m.GetJson()
			j = GS.Send(j, idDoc, "")
			o := j.GetObject()
			_, ok := J.GetValue(o, "errors"); M.Assert(!ok, 103)
			pass := BA.Password()
			d, ok := J.GetValue(o, "data"); M.Assert(ok, 104)
			data, ok := J.GetJson(d); M.Assert(ok, 105)
			idd, ok := J.GetValue(data.GetObject(), "id"); M.Assert(ok, 106)
			id, ok := J.GetJson(idd); M.Assert(ok, 107)
			J.SetValue(id.GetObject(), "password", J.SetString(CB.CryptPass(pass)))
			mk.PushJson(id)
			credentials[i] = &Identity{Nickname: nick, Password: pass}
		}
	}
	mk.BuildArray()
	mk.BuildField("ids")
	mk.BuildObject()
	j := mk.GetJson()
	
	j = GS.Send(j, doc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	if res.Errors == nil {
		out := printAnswer(t, credentials, nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 108)
		return
	}
	out := printFix(t, nicks, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 109)
} //end

func init() {
	W.RegisterPackage(credentialsCreateName, html, end, true)
	W.RegisterPackage(credentialsResetName, html, end, true)
} //init
