/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package web

import (
	
	BA "git.duniter.org/gerard94/skillsclient/basic"
	CB "git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
	SM	"git.duniter.org/gerard94/util/strMapping"
		"encoding/base64"
		"fmt"
		"net/http"
		"os"
		"html/template"
		"time"

)

const (
	
	queryVersion = `
		query Version {
			version
		}
	`
	
	queryIdentityOf = `
		query IdentityOf ($nN: String!) {
			identityOf(nickname: $nN) {
				nickname
			}
		}
	`
	
	queryCreateCredentials = `
		mutation CreateCredentials ($ids: [Identity_input!]!) {
			ids:insertIdentities(ids: $ids) {
				nickname
			}
		}
	`
	
	base = `
		<html>
			<head>{{template "head" .}}</head>
			<body>{{template "body" .}}</body>
		</html>
	`
	
	htmlIndex = `
		{{define "head"}}<title>{{Map "Index"}}</title>{{end}}
		{{define "body"}}
			<h1> SKILLS </h1>
			<h1>{{Map "Index"}}</h1>
			<p>
			{{Map "Creator" "skills"}} gerard94 - CRBxCJrTA6tmHsgt9cQh9SHcCc8w8q95YTp38CPHx2Uk
			</p>
			<p>
				{{Map "Server"}}{{.VersionS}}, {{Map "Client"}}{{.VersionCl}}, {{Map "Tools"}}{{.VersionU}}, {{Map "Crypto"}}{{.VersionCr}}
			</p>
			{{$minI := .MinIndex}}
			{{range $name, $temp := .P}}
				{{if gt $name $minI}}
					<p>
						<a href="/{{$name}}">{{Map $name}}</a> 
					</p>
				{{end}}
			{{end}}
		{{end}}
	`
	
	language_cookie_name = "BO8UK17BZUIO"
	token_cookie_name = "YiieENwKnpJv"
	message_cookie_name = "jfZrFx4P5gQl"
	
	SignInName = "51signIn"
	minNonAdminIndex = "4";
	
	BlockingMaxDuration = 4 * time.Hour
	
	adminsPass = "skillsclient/adminsPass.txt"
	oldAdminsPass = "skillsclient/adminsPass1.txt"
	adminsPassMaxSize = 0x3000

)

type (
	
	SharedData interface {
		Lang () *SM.Lang
		SetLang (string)
		Token () string
		SetToken (string)
		IsAdmin () bool
		ReadMessage () string
		SetMessage (string)
	}
	
	sharedData struct {
		r *http.Request
		w http.ResponseWriter
	}
	
	executeFunc func (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data SharedData)
	
	pack struct {
		tmp string
		call executeFunc
	}

)

var (
	
	packages = make(map[string] *pack)
	packagesD = make(map[string] *pack)
	
	Version string
	
	versionDoc = GS.ExtractDocument(queryVersion)
	idDoc = GS.ExtractDocument(queryIdentityOf)
	credDoc = GS.ExtractDocument(queryCreateCredentials)
	
	onlyUser string = ""
	onlyUserTimer *time.Timer
	
	ap = R.FindDir(adminsPass)
	oldAp = R.FindDir(oldAdminsPass)

)

func (sd *sharedData) Lang () *SM.Lang {
	if c, err := sd.r.Cookie(language_cookie_name); err == nil {
		return SM.NewLanguage(c.Value)
	}
	return SM.NewStdLanguage()
}

func (sd *sharedData) SetLang (l string) {
	http.SetCookie(sd.w, &http.Cookie{Name: language_cookie_name, Value: l})
}

func (sd *sharedData) Token () string {
	if c, err := sd.r.Cookie(token_cookie_name); err == nil {
		return c.Value
	}
	return ""
}

func (sd *sharedData) SetToken (t string) {
	http.SetCookie(sd.w, &http.Cookie{Name: token_cookie_name, Value: t})
}

func (sd *sharedData) IsAdmin () bool {
	_, admin, err := CB.VerifyToken(sd.Token())
	return err == nil && admin
}

func (sd *sharedData) ReadMessage () string {
	if c, err := sd.r.Cookie(message_cookie_name); err == nil {
		v, err := base64.StdEncoding.DecodeString(c.Value); M.Assert(err == nil, err, 100)
		http.SetCookie(sd.w, &http.Cookie{Name: message_cookie_name, Value: ""})
		return string(v)
	}
	return ""
}

func (sd *sharedData) SetMessage (mes string) {
	// Cookie supports ascii characters only
	http.SetCookie(sd.w, &http.Cookie{Name: message_cookie_name, Value: base64.StdEncoding.EncodeToString([]byte(mes))})
}

func RegisterPackage (name, temp string, call executeFunc, displayed bool) {
	p := new(pack)
	p.tmp = temp
	p.call = call
	packages[name] = p
	if displayed {
		packagesD[name] = p
	}
}

func insertAdmins () {
	admins := BA.Admins()
	
	newAdmins := make([]string, 0)
	for _, nick := range admins {
		m := J.NewMaker()
		m.StartObject()
		m.PushString(nick)
		m.BuildField("nN")
		m.BuildObject()
		j := m.GetJson()
		j = GS.Send(j, idDoc, "")
		_, ok := J.GetValue(j.GetObject(), "errors")
		if ok {
			newAdmins = append(newAdmins, nick)
		}
	}
	if len(newAdmins)  == 0 {
		return
	}
	
	type
		identity struct {
			nickname,
			password string
		}
		
	credentials := make([]*identity, len(newAdmins))
	mk := J.NewMaker()
	mk.StartObject()
	mk.StartArray()
	for i, nick := range newAdmins {
		mk.StartObject()
		mk.PushString(nick)
		mk.BuildField("nickname")
		pass := BA.Password()
		mk.PushString(CB.CryptPass(pass))
		mk.BuildField("password")
		mk.BuildObject()
		credentials[i] = &identity{nickname: nick, password: pass}
	}
	mk.BuildArray()
	mk.BuildField("ids")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, credDoc, "")
	_, ok := J.GetValue(j.GetObject(), "errors"); M.Assert(!ok, 100)
	
	fmt.Println("New Admins:")
	for _, id := range credentials {
		fmt.Println(id.nickname, id.password)
	}
	
	fi, err := os.Stat(ap)
	M.Assert(err == nil || os.IsNotExist(err), err, 101)
	var f *os.File
	if err != nil || fi.Size() >= adminsPassMaxSize {
		err := os.Remove(oldAp)
		M.Assert(err == nil || os.IsNotExist(err), err, 102)
		err = os.Rename(ap, oldAp)
		M.Assert(err == nil || os.IsNotExist(err), err, 103)
		f, err = os.Create(ap)
		M.Assert(err == nil, err, 104)
	} else {
		f, err = os.OpenFile(ap, os.O_APPEND | os.O_WRONLY, 0644)
		M.Assert(err == nil, err, 105)
	}
	defer f.Close()
	for _, id := range credentials {
		fmt.Fprintln(f, id.nickname, id.password)
	}
}

func IsOnlyUser () bool {
	return onlyUser != ""
}

func UnsetOnlyUser () {
	if onlyUserTimer != nil {
		onlyUserTimer.Stop()
		onlyUserTimer = nil
	}
	onlyUser = ""
}

func SetOnlyUser (nick string) {
	onlyUser = nick
	onlyUserTimer = time.AfterFunc(BlockingMaxDuration, UnsetOnlyUser)
}

func getHandler (name string, p *pack) http.HandlerFunc {
	
	return func (w http.ResponseWriter, r *http.Request) {
		data := &sharedData{r: r, w: w}
		lang := data.Lang()
		
		t := data.Token()
		nick, _, err := CB.VerifyToken(t)
		if name != SignInName && (err != nil || t == "") { // If the token has not been initialized (t == nil), credentials must be verified
			if t != "" {
				if nick == "" {
					data.SetMessage(lang.Map("#skillsclient:" + err.Error()))
				} else {
					data.SetMessage(fmt.Sprintf("%v: %v", nick, lang.Map("#skillsclient:" + err.Error())))
				}
			}
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		if onlyUser != "" && nick != onlyUser {
			fmt.Fprint(w, lang.Map("#skillsclient:UnderConstruction"))
			return
		}
		
		funcMap := make(template.FuncMap)
		funcMap["Map"] = func (key string, p ...string) string {return lang.Map("#skillsclient:" + key, p...)}
		temp := template.New(name)
		temp = temp.Funcs(funcMap)
		temp = template.Must(temp.Parse(p.tmp))
		temp = template.Must(temp.Parse(base))
		p.call(name, temp, r, w, data)
	}
	
}

func Start () {
	insertAdmins()
	r := http.NewServeMux()
	for name, p := range packages {
		r.HandleFunc("/" + name, getHandler(name, p))
		if name == SignInName {
			r.HandleFunc("/", getHandler(name, p))
		}
	}
	htmlAddress := BA.HtmlAddress()
	server := &http.Server{
		Addr: htmlAddress,
		Handler: r,
	}
	fmt.Println("Listening on", htmlAddress, "...")
	server.ListenAndServe()
}

func manageIndex (name string, temp *template.Template, _ *http.Request, w http.ResponseWriter, sd SharedData) {
	
	type
		output struct {
			VersionU,
			VersionCr,
			VersionS,
			VersionCl string
			P map[string] *pack
			MinIndex string
		}
	
	j := GS.Send(nil, versionDoc, ""); M.Assert(j != nil, 100)
	v, ok := J.GetValue(j.GetObject(), "data"); M.Assert(ok, 101)
	j, ok = J.GetJson(v); M.Assert(ok, 102)
	v, ok = J.GetValue(j.GetObject(), "version"); M.Assert(ok, 103)
	s, ok := J.GetString(v); M.Assert(ok, 104)
	mi := minNonAdminIndex
	if sd.IsAdmin() {
		mi = ""
	}
	err := temp.ExecuteTemplate(w,
		name,
		&output{VersionU: M.Version(),
			VersionCr: CB.Version(),
			VersionS: s,
			VersionCl: Version,
			P: packagesD,
			MinIndex: mi,
		},
	)
	M.Assert(err == nil, err, 103)
}

func init () {
	RegisterPackage("index", htmlIndex, manageIndex, false)
}
