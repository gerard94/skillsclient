/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package credentialsEnter
	
import (
	
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strings"
		"html/template"
		
		/*
		"fmt"
		*/

)

const (
	
	signInName = W.SignInName
	signOutName = "52signOut"
	
	queryLookForPassword = `
		query LookForPassword ($nN: String!) {
			pass: identityOf(nickname: $nN) {
				password
			}
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<h1> SKILLS </h1>
			<h1>{{.Title}}</h1>
			{{if .Prolog}}
				<p>
					{{.Prolog}}
				</p>
			{{end}}
			<p>
			<form action="" method="post">
				{{with .Fix}}
					<p>
						<label for="nickId">{{.NickLbl}}</label>
						<input type="text" name="nick" id="nickId" placeholder="{{.NickPH}}" value="{{.Nick}}" size = 35/>
					</p>
					<p>
						<label for="passId">{{.PassLbl}}</label>
						<input type="password" name="pass" id="passId" placeholder="{{.PassPH}}" value="" size = 35/>
					</p>
				{{end}}
				{{if .Message}}
					<p>
					 {{.Message}}
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
		{{end}}
	`
		
	htmlO = `
		{{define "head"}}{{end}}
		{{define "body"}}
		{{end}}
	`

)

type (
	
	Identity struct {
		Password string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			Pass *Identity
		}
	}
	
	// Outputs
	
	Fix struct {
		NickLbl,
		NickPH,
		Nick,
		PassLbl,
		PassPH string
	}
	
	Out struct {
		Error,
		Prolog,
		Message,
		Title,
		OK string
		Fix *Fix
	}

)

var (
	
	passDoc = GS.ExtractDocument(queryLookForPassword)

)

func doBase (t string, errs []BA.Error, lang *SM.Lang) (title, error, ok string) {
	title = lang.Map(t)
	error = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doFix (nick string, lang *SM.Lang) *Fix {
	nNLbl := lang.Map("#skillsclient:NicknameL", "*") + BA.SpL
	nNPH := lang.Map("#skillsclient:StringPlaceHolder")
	pwLbl := lang.Map("#skillsclient:PassLbl", "*") + BA.SpL
	pwPH := ""
	return &Fix{nNLbl, nNPH, nick, pwLbl, pwPH}
}

func printFix (t, nick, prolog string, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	fix := doFix(nick, lang)
	return &Out{Prolog: prolog, Error: err, Title: t, OK: ok, Fix: fix}
}

func cleanPass (text string) string {
	return strings.TrimSpace(text)
} //cleanPass

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == signInName, name, 100)
	t := "#skillsclient:CredentialsEnter"
	
	if r.Method == "GET" {
		out := printFix(t, "", data.ReadMessage(), nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	
	r.ParseForm()
	nick := r.PostFormValue("nick")
	pass := cleanPass(r.PostFormValue("pass"))
	
	if nick == "" || pass == "" {
		out := printFix(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
		return
	}
	
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(nick)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	
	j = GS.Send(j, passDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	if res.Errors != nil {
		out := printFix(t, nick, "", res.Errors, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 104)
		return
	}
	
	if !CB.VerifyPass(pass, res.Data.Pass.Password) {
		out := &Out{Message: lang.Map("#skillsclient:WrongCredentials"), OK: lang.Map("#skillsclient:OK")}
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 104)
		return
	}
	
	admin := BA.IsAdmin(nick)
	tok := CB.MakeToken(nick, admin)
	data.SetToken(tok)
	http.Redirect(w, r, "/index", http.StatusFound)
} //end

func signOut (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	data.SetToken("")
	http.Redirect(w, r, "/", http.StatusFound)
} //signOut

func init() {
	W.RegisterPackage(signInName, html, end, true)
	W.RegisterPackage(signOutName, htmlO, signOut, true)
} //init
