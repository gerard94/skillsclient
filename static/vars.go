/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package static

// Package strMapping implements a mapping between keys and longer strings.

var (
	
	strEn = `STRINGS

Add_Info	Additional informations:
AddLoc	Add Country / Region
AddSer	Add Category / Speciality
BothLbl	Both
CaRen	Rename Category
CategoryLbl	New Category:
CategoryRen	Rename Category
221categoryRename	Manager: Rename Categories
123citiesRename	Manager: Rename Cities
CityRen	Rename City
Client	Client Version: 
CoRen	Rename Country
CountryLbl	New Country:
CountryRen	Rename Country
121countryRename	Manager: Rename Countries
Creator	Creator of ^0:
CredentialsCreate	Create Credentials
311credentialsCreate	Manager: Create Credentials
CredentialsReset	Reset Credentials
312credentialsReset	Manager: Reset Credentials
CredentialsEnter	Enter Credentials
Crypto	Crypto Version:
CyRen	Rename City
dontExistCa	Category ^0 doesn't exist in the base
dontExistCo	Country ^0 doesn't exist in the base
dontExistId	Nickname ^0 doesn't exist in the base
dontExist2Loc	Location ^0/^1 doesn't exist in the base
dontExist2Ser	Service ^0/^1 doesn't exist in the base
Email	Email:
en	English
existsId	nickname ^0 already exists in the base
exists2Loc	Location ^0 / ^1 already exists in the base
exists2Ser	Service ^0 / ^1 already exists in the base
ExpiredToken	Your session has expired, please sign in again
fr	Français
G1_nickname	Ğ1 Nickname:
HintLbl	Nickname or prefix:
HintPlaceHolder	Type the beginning of a nickname
IdentitiesScan	Scan Identities
42identitiesScan	Scan Identities
IdentitiesSearch	Search Services
41identitiesSearch	Search Services
IdentityChange	Change your own Identity
32identityChange	Manager: Change Identity
43identityChange	Change your own Identity
IdentityChangeM	Change Identity
IdentityRemove	Remove Identity
33identityRemove	Manager: Remove Identity
idInLoc	There are identities in location ^0 / ^1
idInSer	There are identities in service ^0 / ^1
IdListLbl	List of nicknames:
Index	Index
index	index
inVoidCa	Speciality ^0 in void category
inVoidCo	Region ^0 in void country
inVoidRg	City ^0 in void region
language	Choose Language
53language	Choose Language
Locations	Locations^0:
LocationsIns	Insert Locations
11locationInsert	Manager: Insert Locations
LocationRem	Remove Locations
13locationRemove	Manager: Remove Locations
LockSystem	Lock System
34lockSystem	Manager: Lock System
LocList	Locations List:
LocRem	Remove Country / Region
NewCategoryLbl	New Category Name:
NewCityLbl	New City Name:
NewCountryLbl	New Country Name:
newPassLbl	New Password: 
newPassLbl2	Retype new Password: 
NewRegionLbl	New Region Name:
NewSpecialityLbl	New Speciality Name:
NicknameL	Nickname^0:
Nicknames	Nicknames^0:
nnNull	New name is void
No	No
OK	OK
passDiff	Passwords are different
PassLbl	Password^0: 
PasswordChange	Change Password
44passwordChange	Change Password
Pickup	Can store goods?
Pubkey	Ğ1 Public Key^0:
Recorded	Your data have been recorded
RegionLbl	New Region:
RegionRen	Rename Region
122regionRename	Manager: Rename Regions
RemDone	^0's form removed
RemNotice	Warning: You're about to delete ^0's form
RgRen	Rename Region
Searched_services	Researched Services:
SeLbl	Searched
Select	Select:
SerList	Services List:
SerRem	Remove Category / Speciality
Server	Server Version: 
21serviceInsert	Manager: Insert Services
ServiceRem	Remove Services
23serviceRemove	Manager: Remove Services
ServicesIns	Insert Services
SesuLbl	Do you look for supplied or for searched services?
51signIn	Change User
52signOut	Sign out
SpecialityLbl	New Speciality:
SpecialityRen	Rename Speciality
222specialityRename	Manager: Rename Specialities
SpRen	Rename Speciality
StringPlaceHolder	Type a text
SuLbl	Supplied
Supplied_services	Supplied Services^0:
SystemLocked	The system is locked for everybody but ^0 until at most ^1
SystemUnlocked	The system is unlocked
Tools	Tools Version: 
Transport	Can transport goods?
TypeCategory	Type a category
TypeCities	Type the cities where you operate in ^0 / ^1 (line by line):
TypeCity	Type a city
TypeCountry	Type a country
TypeRegion	Type a region
TypeSpeciality	Type a speciality
UnderConstruction	The service is under construction; retry later
UnlockSystem	Unlock System
voidCa	Void category
voidCo	Void country
voidCy	Void city
voidNN	Void nickname
voidPwd	Void password
voidRg	Void region
voidSp	Void speciality
WrongCredentials	Incorrect nickname or password
WrongToken	Wrong Token
Yes	Yes
`
	strFr = `STRINGS

Add_Info	Autres informations:
AddLoc	Insérer ce lieu
AddSer	Insérer ce service
BothLbl	Les deux
CaRen	Renommer la catégorie
CategoryLbl	Nouvelle catégorie :
CategoryRen	Renommer des catégories
221categoryRename	Gestionnaire : Renommer des catégories
123citiesRename	Gestionnaire : Renommer des villes
CityRen	Renommer des villes
Client	Version client : 
CoRen	Renommer le pays
CountryLbl	Nouveau pays :
CountryRen	Renommer des pays
121countryRename	Gestionnaire : Renommer des pays
Creator	Createur de ^0 :
CredentialsCreate	Création d'identifiants
311credentialsCreate	Gestionnaire : Création d'identifiants
CredentialsReset	Renouvellement d'identifiants
312credentialsReset	Gestionnaire : Renouvellement d'identifiants
CredentialsEnter	Vérification des identifiants
Crypto	Version crypto :
CyRen	Renommer la ville
dontExistCa	La catégorie ^0 n'existe pas dans la base
dontExistCo	Le pays ^0 n'existe pas dans la base
dontExistId	Le pseudo ^0 n'existe pas dans la base
dontExist2Loc	Le lieu ^0/^1 n'existe pas dans la base
dontExist2Ser	Le service ^0/^1 n'existe pas dans la base
Email	Courriel :
en	English
existsId	Le pseudo ^0 existe déjà dans la base
exists2Loc	Le lieu ^0 / ^1 existe déjà dans la base
exists2Ser	Le service ^0 / ^1 existe déjà dans la base
ExpiredToken	Votre session est terminée, veuillez renseigner à nouveau vos identifiants
fr	Français
G1_nickname	Pseudo Ğ1 :
HintLbl	Début de pseudo :
HintPlaceHolder	Entrez un pseudo ou un début de pseudo
IdentitiesScan	Parcourir les fiches
42identitiesScan	Parcourir les fiches
IdentitiesSearch	Recherche de services
41identitiesSearch	Recherche de services
IdentityChange	Modifier sa fiche
32identityChange	Gestionnaire : Modifier des fiches
43identityChange	Modifier sa fiche
IdentityChangeM	Modifier une fiche
IdentityRemove	Supprimer des fiches
33identityRemove	Gestionnaire : Supprimer des fiches
idInLoc	Il y a des fiches dans ce lieu : ^0 / ^1
idInSer	Il y a des fiches dans ce service : ^0 / ^1
IdListLbl	Liste des pseudos :
Index	Menu
index	menu
inVoidCa	La spécialité ^0 n'a pas de catégorie
inVoidCo	Le département ^0 n'a pas de pays
inVoidRg	La ville ^0 n'a pas de département
language	Choix de la langue
53language	Choix de la langue
Locations	Lieux^0 :
LocationsIns	Ajouter des lieux
11locationInsert	Gestionnaire : Ajouter des lieux
LocationRem	Supprimer des lieux
13locationRemove	Gestionnaire : Supprimer des lieux
LockSystem	Bloquer le système
34lockSystem	Gestionnaire : Bloquer le système
LocList	Liste des lieux :
LocRem	Supprimer ce lieu
NewCategoryLbl	Nouveau nom de la catégorie :
NewCityLbl	Nouveau nom de la ville :
NewCountryLbl	Nouveau nom du pays :
newPassLbl	Nouveau mot de passe : 
newPassLbl2	Retaper le nouveau mot de passe : 
NewRegionLbl	Nouveau nom du département :
NewSpecialityLbl	Nouveau nom de la spécialité :
NicknameL	Pseudo^0 :
Nicknames	Pseudos^0 :
nnNull	Le nouveau nom est vide
No	Non
OK	OK
passDiff	Les deux mots de passe sont differents
PassLbl	Mot de passe^0 : 
PasswordChange	Changer de mot de passe
44passwordChange	Changer de mot de passe
Pickup	Peut faire dépôt de marchandises ?
Pubkey	Clef publique Ğ1^0 :
Recorded	Vos données ont été enregistrées
RegionLbl	Nouveau département :
RegionRen	Renommer des départements
122regionRename	Gestionnaire : Renommer des départements
RemDone	La fiche de ^0 a été supprimée
RemNotice	Attention : Vous êtes sur le point de supprimer la fiche de ^0
RgRen	Renommer le département
Searched_services	Services recherchés :
SeLbl	Recherchés
Select	Sélectionner :
SerList	Liste des Services :
SerRem	Supprimer le service
Server	Version serveur : 
21serviceInsert	Gestionnaire : Ajouter des services
ServiceRem	Supprimer des services
23serviceRemove	Gestionnaire : Supprimer des services
ServicesIns	Ajouter des services
SesuLbl	Êtes-vous intéressés par des services fournis ou recherchés ?
51signIn	Changer d'utilisateur
52signOut	Déconnexion
SpecialityLbl	Nouvelle spécialité :
SpecialityRen	Renommer des spécialités
222specialityRename	Gestionnaire : Renommer des spécialités
SpRen	Renommer la spécialité
StringPlaceHolder	Entrez un texte
SuLbl	Fournis
Supplied_services	Services fournis^0 :
SystemLocked	Le système est bloqué pour tous sauf ^0 jusqu'à ^1 au plus tard
SystemUnlocked	Le système est débloqué
Tools	Version outils : 
Transport	Peut transporter des marchandises ?
TypeCategory	Entrez une catégorie
TypeCities	Entrez les villes où vous intervenez dans ^0 / ^1 (ligne par ligne) :
TypeCity	Entrez une ville
TypeCountry	Entrez un pays
TypeRegion	Entrez un département
TypeSpeciality	Entrez une spécialité
UnderConstruction	Le service est en travaux ; veuillez réessayer plus tard
UnlockSystem	Débloquer le système
voidCa	Categorie vide
voidCo	Pays vide
voidCy	Ville vide
voidNN	Pseudo vide
voidPwd	Mot de passe vide
voidRg	Département vide
voidSp	Spécialité vide
WrongCredentials	Pseudo ou mot de passe incorrect
WrongToken	Jeton incorrect
Yes	Oui
`
)
