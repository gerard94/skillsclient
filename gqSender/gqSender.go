/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package gqSender

import (
	
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	G	"git.duniter.org/gerard94/util/graphQL"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
		"net/http"
		"io/ioutil"
		"strings"
		"time"

)

const (
	
	errNoServer = 1
	
	sendSleepTime = 1 * time.Second

)

func send (request string) J.Json {
	r, err := http.Post(BA.ServerAddress(), "application/json", strings.NewReader(request))
	for err != nil {
		time.Sleep(sendSleepTime)
		r, err = http.Post(BA.ServerAddress(), "application/json", strings.NewReader(request))
	}
	M.Assert(r.StatusCode / 100 == 2, r.StatusCode, 101)
	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	M.Assert(err == nil, err, 102)
	return J.ReadString(string(b))
} //send

func Send (j J.Json, doc *G.Document, op string) J.Json {
	mk := J.NewMaker()
	mk.StartObject()
	if j != nil {
		mk.PushJson(j)
		mk.BuildField("variables")
	}
	s := doc.GetFlatString()
	mk.PushString(s)
	mk.BuildField("query")
	if op != "" {
		mk.PushString(op)
		mk.BuildField("operationName")
	}
	mk.BuildObject()
	j = send(mk.GetJson().GetFlatString())
	M.Assert(j != nil, 100)
	return j
} //Send

func ExtractDocument (docString string) *G.Document {
	doc, err := G.Compile(strings.NewReader(docString))
	M.Assert(doc != nil, err, 100)
	return doc
} //ExtractDocument
