/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package credentialsCreate
	
import (
	
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strings"
		"html/template"
		
		/*
		"fmt"
		*/

)

const (
	
	changePassName = "44passwordChange"
	
	queryIdentityOf = `
		query IdentityOf ($nN: String!) {
			id:identityOf(nickname: $nN) {
				nickname
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	queryChangePassword = `
		mutation ChangePassword ($nN: String!, $id: Identity_input!) {
			id:changeIdentity(nickname: $nN, newId: $id) {
				nickname
			}
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .Fix}}
					<p>
						{{.Nickname}}
					</p>
					<p>
						<label for="pass1Id">{{.Pass1Lbl}}</label>
						<input type="password" name="pass1" id="pass1Id" placeholder="{{.PassPH}}" value="{{.Password}}" size = 35/>
					</p>
					<p>
						<label for="pass2Id">{{.Pass2Lbl}}</label>
						<input type="password" name="pass2" id="pass2Id" placeholder="{{.PassPH}}" value="{{.Password}}" size = 35/>
					</p>
				{{end}}
				{{if .Message}}
					<p>
					 {{.Message}}
					</p>
					<input type="hidden" name="message" value="done"/>
				{{end}}
				{{if .Warning}}
					<p>
					 {{.Warning}}
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Region struct {
		Region string
		Cities []string
	}
	
	Location struct {
		Country string
		Regions []Region
	}
	
	Service struct {
		Category string
		Specialities []string
	}
	
	Identity struct {
		Nickname,
		Password,
		G1_nickname,
		Pubkey,
		Email string
		Locations []Location
		Transport,
		Pickup bool
		Supplied_services,
		Searched_services []Service
		Additional_informations string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			Id *Identity
		}
	}
	
	// Outputs
	
	Fix struct {
		Nickname,
		Pass1Lbl,
		Pass2Lbl,
		PassPH,
		Password string
	}
	
	Out struct {
		Error,
		Message,
		Warning,
		Title,
		OK string
		Fix *Fix
	}

)

var (
	
	idDoc = GS.ExtractDocument(queryIdentityOf)
	passDoc = GS.ExtractDocument(queryChangePassword)

)

func doBase (t string, errs []BA.Error, lang *SM.Lang) (title, error, ok string) {
	title = lang.Map(t)
	error = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doFix (nick, pass string, lang *SM.Lang) *Fix {
	nN := lang.Map("#skillsclient:NicknameL", "") + BA.SpL + nick
	pw1Lbl := lang.Map("#skillsclient:newPassLbl") + BA.SpL
	pw2Lbl := lang.Map("#skillsclient:newPassLbl2") + BA.SpL
	pwPH := ""
	return &Fix{nN, pw1Lbl, pw2Lbl, pwPH, pass}
}

func printFix (t, nick, pass, warn, msg string, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(t, errs, lang)
	fix := doFix(nick, pass, lang)
	return &Out{Error: err, Message: msg, Warning: warn, Title: t, OK: ok, Fix: fix}
}

func cleanPass (text string) string {
	return strings.TrimSpace(text)
} //cleanPass

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == changePassName, name, 20)
	t := "#skillsclient:PasswordChange"
	
	r.ParseForm()
	mes := r.PostFormValue("message")
	if mes != "" {
		http.Redirect(w, r, "/index", http.StatusFound)
		return
	}
	
	nick, _, _ := CB.VerifyToken(data.Token())
	
	if r.Method == "GET" {
		out := printFix(t, nick, "", "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	
	pass := cleanPass(r.PostFormValue("pass1"))
	pass2 := cleanPass(r.PostFormValue("pass2"))
	
	if pass != pass2 {
		out := printFix(t, nick, "",  lang.Map("#skillsclient:passDiff"), "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 109)
		return
	}
	if pass == "" {
		http.Redirect(w, r, "/index", http.StatusFound)
		return
	}
	
	
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(nick)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, idDoc, "")
	o := j.GetObject()
	_, ok := J.GetValue(o, "errors"); M.Assert(!ok, 103)
	d, ok := J.GetValue(o, "data"); M.Assert(ok, 104)
	datas, ok := J.GetJson(d); M.Assert(ok, 105)
	idd, ok := J.GetValue(datas.GetObject(), "id"); M.Assert(ok, 106)
	id, ok := J.GetJson(idd); M.Assert(ok, 107)
	J.SetValue(id.GetObject(), "password", J.SetString(CB.CryptPass(pass)))
	mk = J.NewMaker()
	mk.StartObject()
	mk.PushString(nick)
	mk.BuildField("nN")
	mk.PushJson(id)
	mk.BuildField("id")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, passDoc, "")
	o = j.GetObject()
	if _, ok := J.GetValue(o, "errors"); ok {
		res := new(Res)
		J.ApplyTo(j, res)
		out := printFix(t, nick, pass, "", "", res.Errors, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 109)
	}
	out := &Out{Message: lang.Map("#skillsclient:Recorded"), OK: lang.Map("#skillsclient:OK")}
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 106)
} //end

func init() {
	W.RegisterPackage(changePassName, html, end, true)
} //init
