/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package scanIdentities

import (
	
	BA	 "git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"html/template"
		/*
		"fmt"
		*/

)

const (
	
	identitiesScanName = "42identitiesScan"
	
	queryListIdentities = `
		query ListIdentities ($h: String) {
			idList: allIdentities(hint: $h) {
				nickname
			}
		}
	`
	
	queryIdentityOf = `
		query IdentityOf ($nN: String!) {
			id:identityOf(nickname: $nN) {
				nickname
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .Start}}
					<p>
						<label for="hintId">{{.HintLbl}}</label>
						<input type="text" name="hint" id="hintId" placeholder="{{.HintPH}}" value="{{.Hint}}" size = 35/>
					</p>
					<input type="hidden" name="oldHint" value="{{.Hint}}"/>
				{{end}}
				{{with .Find}}
					<p>
						<label for="idListId">{{.IdListLbl}}</label>
						{{$selN := .SelectedNn}}
						<select name="idList" id="idListId" >
							{{range $nick:= .Nicks}}
								<option value="{{$nick}}" {{if eq $nick $selN}}selected{{end}}>{{$nick}}</option>
							{{end}}
						</select>
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{with .Fix}}
				<p>
					{{.Nickname}}
				</p>
				<p>
					{{.G1_nickname}}
				</p>
				<p>
					{{.Pubkey}}
				</p>
				<p>
					{{.Email}}
				</p>
				<p>
					{{.Locations}}
					<blockquote>
					{{range $co := .Locs}}
						{{range $rg := $co.Regions}}
							{{if eq (len $rg.Cities) 0}}
								{{$co.Country}} / {{$rg.Region}}
								<br>
							{{else}}
								{{range $cy := $rg.Cities}}
									{{$co.Country}} / {{$rg.Region}} / {{$cy}}
									<br>
								{{end}}
							{{end}}
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Transport}}
				</p>
				<p>
					{{.Pickup}}
				</p>
				<p>
					{{.Supplied_services}}
					<blockquote>
					{{range $ca := .Su_ser}}
						{{range $sp := $ca.Specialities}}
							{{$ca.Category}} / {{$sp}}
							<br>
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Searched_services}}
					<blockquote>
					{{range $ca := .Se_ser}}
						{{range $sp := $ca.Specialities}}
							{{$ca.Category}} / {{$sp}}
							<br>
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Additional_informations}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Region struct {
		Region string
		Cities []string
	}
	
	Location struct {
		Country string
		Regions []Region
	}
	
	Service struct {
		Category string
		Specialities []string
	}
	
	Identity struct {
		Nickname,
		G1_nickname,
		Pubkey,
		Email string
		Locations []Location
		Transport,
		Pickup bool
		Supplied_services,
		Searched_services []Service
		Additional_informations string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			IdList []*Identity
			Id *Identity
		}
	}
	
	// Outputs
	
	Start struct {
		HintLbl,
		HintPH,
		Hint string
	}
	
	Find struct {
		IdListLbl,
		SelectedNn string
		Nicks []string
	}
	
	Fix struct {
		Nickname,
		G1_nickname,
		Pubkey,
		Email,
		Locations,
		Transport,
		Pickup,
		Supplied_services,
		Searched_services,
		Additional_informations string
		Locs []Location
		Su_ser,
		Se_ser []Service
	}
	
	Out struct {
		Error,
		Title,
		OK string
		Start *Start
		Find *Find
		Fix	*Fix
	}

)

var (
	
	idListDoc = GS.ExtractDocument(queryListIdentities)
	idDoc = GS.ExtractDocument(queryIdentityOf)

)

func doBase (t string, errs []BA.Error, lang *SM.Lang) (title, error, ok string) {
	title = lang.Map(t)
	error = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doStart (hint string, lang *SM.Lang) *Start {
	hl := lang.Map("#skillsclient:HintLbl") + BA.SpL
	hp := lang.Map("#skillsclient:HintPlaceHolder")
	return &Start{hl, hp, hint}
}

func printStart (title, hint string, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	start := doStart(hint, lang)
	return &Out{Error: err, Title: t, OK: ok, Start: start}
}

func doFind (selNN string, idList []*Identity, lang *SM.Lang) *Find {
	if len(idList) == 0 {
		return nil
	}
	idlLbl := lang.Map("#skillsclient:IdListLbl") + BA.SpL
	nicks := make([]string, len(idList))
	for i, id := range idList {
		nicks[i] = id.Nickname
	}
	return &Find{idlLbl, selNN, nicks}
}

func printFind (title, hint, selNN string, idList []*Identity, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	start := doStart(hint, lang)
	find := doFind(selNN, idList, lang)
	return &Out{Error: err, Title: t, OK: ok, Start: start, Find: find}
}

func doFix (id *Identity, lang *SM.Lang) *Fix {
	
	yesNo := func (b bool) string {
		if b {
			return lang.Map("#skillsclient:Yes")
		}
		return lang.Map("#skillsclient:No")
	}
	
	if id == nil {
		return nil
	}
	nN := lang.Map("#skillsclient:NicknameL", "") + BA.SpL + id.Nickname
	gNN := lang.Map("#skillsclient:G1_nickname") + BA.SpL + id.G1_nickname
	pub := lang.Map("#skillsclient:Pubkey", "") + BA.SpL + id.Pubkey
	mail := lang.Map("#skillsclient:Email") + BA.SpL + id.Email
	locations := lang.Map("#skillsclient:Locations", "")
	transport := lang.Map("#skillsclient:Transport") + BA.SpL + yesNo(id.Transport)
	pickup := lang.Map("#skillsclient:Pickup") + BA.SpL + yesNo(id.Pickup)
	sup := lang.Map("#skillsclient:Supplied_services", "")
	sea := lang.Map("#skillsclient:Searched_services")
	add := lang.Map("#skillsclient:Add_Info") + BA.SpL + id.Additional_informations
	return &Fix{nN, gNN, pub, mail, locations, transport, pickup, sup, sea, add, id.Locations, id.Supplied_services, id.Searched_services}
}

func printFix (title, hint, selNN string, idList []*Identity, id *Identity, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	start := doStart(hint, lang)
	find := doFind(selNN, idList, lang)
	fix := doFix(id, lang)
	return &Out{Error: err, Title: t, OK: ok, Start: start, Find: find, Fix: fix}
} //print

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == identitiesScanName, name, 100)
	t := "#skillsclient:IdentitiesScan"
	if r.Method == "GET" {
		out := printStart(t, "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	hint := r.PostFormValue("hint")
	oldHint := r.PostFormValue("oldHint")
	selNN := r.PostFormValue("idList")
	isFix := hint == oldHint && selNN != ""
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(hint)
	mk.BuildField("h")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, idListDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	M.Assert(res.Errors == nil, 102)
	if !isFix {
		M.Assert(res.Data.IdList != nil, 103)
		if len(res.Data.IdList) != 1 {
			out := printFind(t, hint, selNN, res.Data.IdList, nil, lang)
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 104)
			return
		}
		selNN = res.Data.IdList[0].Nickname
	}
	mk = J.NewMaker()
	mk.StartObject()
	mk.PushString(selNN)
	mk.BuildField("nN")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, idDoc, "")
	J.ApplyTo(j, res)
	out := printFix(t, hint, selNN, res.Data.IdList, res.Data.Id, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 105)
} //end

func init() {
	W.RegisterPackage(identitiesScanName, html, end, true)
} //init
