/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package cities

import (
	
	BA	 "git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strings"
		"html/template"
		/*
		"fmt"
		*/

)

const (
	
	citiesRenameName = "123citiesRename"
	
	queryListCities = `
		query ListCities ($co: String, $rg: String) {
			locList:locationList(country: $co, region: $rg) {
				country
				region
				city
			}
		}
	`
	
	queryRenameCity = `
		mutation CityRename ($co: String!, $rg: String!, $cy: String!, $nN: String!) {
			renameLocation(location: CITY, country: $co, region: $rg, city: $cy, newName: $nN) 
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .List}}
					<p>
						<label for="locList">{{.LocListLbl}}</label>
						{{$coPos := .CoPos}}
						{{$rgPos := .RgPos}}
						{{$cyPos := .CyPos}}
						<select name="locList" id="locList" >
							{{range $loc := .Locations}}
								{{if ne $loc.City ""}}
									<option value="{{$loc.Country}}|{{$loc.Region}}|{{$loc.City}}" {{if and (eq $loc.Country $coPos) (eq $loc.Region $rgPos) (eq $loc.City $cyPos)}}selected{{end}}>{{$loc.Country}} / {{$loc.Region}} / {{$loc.City}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
				{{end}}
				{{with .RenameCy}}
					<p>
						<label for="newCityId">{{.NewCyNameLbl}}</label>
						<input type="text" name="newCity" id="newCityId" placeholder="{{.PlaceholderNC}}" value="" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.CyRen}}">
					</p>
				{{end}}
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Location1 struct {
		Country,
		Region,
		City string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			LocList []Location1
			CyRen bool
		}
	}
	
	// Outputs
	
	List struct {
		LocListLbl,
		CoPos,
		RgPos,
		CyPos string
		Locations []Location1
	}
	
	RenameCy struct {
		NewCyNameLbl,
		PlaceholderNC,
		CyRen string
	}
	
	Out struct {
		List *List
		RenameCy *RenameCy
		Title,
		Error string
	}

)

var (
	
	listLocDoc = GS.ExtractDocument(queryListCities)
	cyRenDoc = GS.ExtractDocument(queryRenameCity)

)

func doList (coPos, rgPos, cyPos string, lang *SM.Lang) *List {
	lLbl := lang.Map("#skillsclient:LocList") + BA.SpL
	j := GS.Send(nil, listLocDoc, "")
	_, b := J.GetValue(j.GetObject(), "errors"); M.Assert(!b, j.GetString(), 100)
	res := new(Res)
	J.ApplyTo(j, res)
	locs := res.Data.LocList
	return &List{lLbl, coPos, rgPos, cyPos, locs}
} //doList

func doRenameCy (lang *SM.Lang) *RenameCy {
	ncLbl := lang.Map("#skillsclient:NewCityLbl") + BA.SpL
	phC := lang.Map("#skillsclient:TypeCity")
	cyRen := lang.Map("#skillsclient:CyRen")
	return &RenameCy{ncLbl, phC, cyRen}
} //doRenameCy

func printCyRen (title, coPos, rgPos, cyPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(coPos, rgPos, cyPos, lang)
	renameCy := doRenameCy(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, RenameCy: renameCy, Title: t, Error: err}
} //printCyRen

func endCyRen (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == citiesRenameName, name, 100)
	t := "#skillsclient:CityRen"
	if r.Method == "GET" {
		out := printCyRen(t, "", "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	newCity := r.PostFormValue("newCity")
	coRgCy := r.PostFormValue("locList")
	l := strings.Split(coRgCy, "|")
	M.Assert(len(l) == 3, len(l), 102)
	country := l[0]
	region := l[1]
	oldCity := l[2]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(country)
	mk.BuildField("co")
	mk.PushString(region)
	mk.BuildField("rg")
	mk.PushString(oldCity)
	mk.BuildField("cy")
	mk.PushString(newCity)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, cyRenDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	out := printCyRen(t, country, region, newCity, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 103)
} //endCyRen

func init() {
	W.RegisterPackage(citiesRenameName, html, endCyRen, true)
} //init
