/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package main

import (
	
	_	"git.duniter.org/gerard94/util/babel/static"
	_	"git.duniter.org/gerard94/util/json/static"
	_	"git.duniter.org/gerard94/util/graphQL/static"
	_	"git.duniter.org/gerard94/skillsclient/static"
	
	CB	"git.duniter.org/gerard94/skillsKeys/cryptoBasic"
	M	"git.duniter.org/gerard94/util/misc"
	W	"git.duniter.org/gerard94/skillsclient/web"
	
	// language
	_	"git.duniter.org/gerard94/skillsclient/language"
	_	"git.duniter.org/gerard94/skillsclient/locations"
	_	"git.duniter.org/gerard94/skillsclient/cities"
	_	"git.duniter.org/gerard94/skillsclient/services"
	_	"git.duniter.org/gerard94/skillsclient/scanIdentities"
	_	"git.duniter.org/gerard94/skillsclient/identityChange"
	_	"git.duniter.org/gerard94/skillsclient/identityChangeM"
	_	"git.duniter.org/gerard94/skillsclient/identityRemove"
	_	"git.duniter.org/gerard94/skillsclient/identitiesSearch"
	_	"git.duniter.org/gerard94/skillsclient/credentialsCreate"
	_	"git.duniter.org/gerard94/skillsclient/signIn"
	_	"git.duniter.org/gerard94/skillsclient/lockSystem"
	_	"git.duniter.org/gerard94/skillsclient/changePassword"
	
		"fmt"

)

const (
	
	version = "1.1.4"

)

func main () {
	if !CB.GetKey() {
		return
	}
	fmt.Println("skills Client Version", version, "Tools Version", M.Version(), "Crypto Version", CB.Version(), "\n")
	W.Version = version
	W.Start()
}
