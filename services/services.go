/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package services

import (
	
	BA	 "git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strings"
		"html/template"
		/*
		"fmt"
		*/

)

const (
	
	serviceInsertName = "21serviceInsert"
	categoryRenameName = "221categoryRename"
	specialityRenameName = "222specialityRename"
	serviceRemoveName = "23serviceRemove"
	
	queryListServices = `
		query ListServices ($ca: String) {
			serList: allServices(category: $ca) {
				category
				specialities
			}
		}
	`
	
	queryInsertService = `
		mutation InsertService ($ca: String!, $sp: String!) {
			serIns: insertService (category: $ca, speciality: $sp) {
				category
				speciality
			}
		}
	`
	
	queryRenameCategory = `
		mutation RenameCategory ($ca: String!, $sp: String, $nN: String!) {
			caRen: renameService(service: CATEGORY, category: $ca, speciality: $sp, newName: $nN)
		}
	`
	
	queryRenameSpeciality = `
		mutation RenameSpeciality ($ca: String!, $sp: String!, $nN: String!) {
			spRen: renameService(service: SPECIALITY, category: $ca, speciality: $sp, newName: $nN)
		}
	`
	
	queryRemoveService = `
		mutation RemoveService ($ca: String!, $sp: String!) {
			serRem: removeService(category: $ca, speciality: $sp)
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .List}}
					<p>
						<label for="serList">{{.SerListLbl}}</label>
						{{$caPos := .CaPos}}
						{{$spPos := .SpPos}}
						<select name="serList" id="serList" >
							{{range $ca := .Services}}
								{{range $sp := $ca.Specialities}}
									<option value="{{$ca.Category}}|{{$sp}}" {{if and (eq $ca.Category $caPos) (eq $sp $spPos)}}selected{{end}}>{{$ca.Category}} / {{$sp}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
				{{end}}
				{{with .Insert}}
					<p>
						<label for="categoryId">{{.CategoryLbl}}</label>
						<input type="text" name="category" id="categoryId" placeholder="{{.PlaceholderC}}" value="{{.Category}}" size = 35/>
					</p>
					<p>
						<label for="specialityId">{{.SpecialityLbl}}</label>
						<input type="text" name="speciality" id="specialityId" placeholder="{{.PlaceholderS}}" value="{{.Speciality}}" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.AddSer}}">
					</p>
				{{end}}
				{{with .RenameCa}}
					<p>
						<label for="newCategoryId">{{.NewCaNameLbl}}</label>
						<input type="text" name="newCategory" id="newCategoryId" placeholder="{{.PlaceholderNC}}" value="" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.CaRen}}">
					</p>
				{{end}}
				{{with .RenameSp}}
					<p>
						<label for="newSpecialityId">{{.NewSpNameLbl}}</label>
						<input type="text" name="newSpeciality" id="newSpecialityId" placeholder="{{.PlaceholderNS}}" value="" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.SpRen}}">
					</p>
				{{end}}
				{{with .Remove}}
					<p>
						<input type="submit" value="{{.SerRem}}">
					</p>
				{{end}}
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Service struct {
		Category string
		Specialities []string
	}
	
	Service1 struct {
		Category,
		Speciality string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			SerList []*Service
			SerIns *Service1
			CaRen,
			SpRen,
			SerRem bool
		}
	}
	
	// Outputs
	
	List struct {
		SerListLbl,
		CaPos,
		SpPos string
		Services []*Service
	}
	
	Insert struct {
		CategoryLbl,
		PlaceholderC,
		Category,
		SpecialityLbl,
		PlaceholderS,
		Speciality,
		AddSer string
	}
	
	RenameCa struct {
		NewCaNameLbl,
		PlaceholderNC,
		CaRen string
	}
	
	RenameSp struct {
		NewSpNameLbl,
		PlaceholderNS,
		SpRen string
	}
	
	Remove struct {
		SerRem string
	}
	
	Out struct {
		List *List
		Insert *Insert
		RenameCa *RenameCa
		RenameSp *RenameSp
		Remove *Remove
		Title,
		Error string
	}

)

var (
	
	listSerDoc = GS.ExtractDocument(queryListServices)
	insSerDoc = GS.ExtractDocument(queryInsertService)
	renCaDoc = GS.ExtractDocument(queryRenameCategory)
	renSpDoc = GS.ExtractDocument(queryRenameSpeciality)
	remSerDoc = GS.ExtractDocument(queryRemoveService)

)

func doList (caPos, spPos string, lang *SM.Lang) *List {
	lLbl := lang.Map("#skillsclient:SerList") + BA.SpL
	j := GS.Send(nil, listSerDoc, "")
	if _, b := J.GetValue(j.(*J.Object), "errors"); b {
		M.Halt(j.GetString(), 100)
	}
	res := new(Res)
	J.ApplyTo(j, res)
	sers := res.Data.SerList
	return &List{lLbl, caPos, spPos, sers}
} //doList

func doInsert (ca, sp string, lang *SM.Lang) *Insert {
	cLbl := lang.Map("#skillsclient:CategoryLbl") + BA.SpL
	phC := lang.Map("#skillsclient:TypeCategory")
	sLbl := lang.Map("#skillsclient:SpecialityLbl") + BA.SpL
	phS := lang.Map("#skillsclient:TypeSpeciality")
	addSer := lang.Map("#skillsclient:AddSer")
	return &Insert{cLbl, phC, ca, sLbl, phS, sp, addSer}
} //doInsert

func printIns (title, caPos, ca, spPos, sp string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(caPos, spPos, lang)
	insert := doInsert(ca, sp, lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, Insert: insert, Title: t, Error: err}
} //printIns

func endIns (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == serviceInsertName, name, 100)
	t := "#skillsclient:ServicesIns"
	if r.Method == "GET" {
		out := printIns(t, "", "", "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	category := r.PostFormValue("category")
	speciality := r.PostFormValue("speciality")
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(category)
	mk.BuildField("ca")
	mk.PushString(speciality)
	mk.BuildField("sp")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, insSerDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	ca := ""; sp := ""
	if res.Errors != nil {
		ca = category
		sp = speciality
	}
	out := printIns(t, category, ca, speciality, sp, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //endIns

func doRenameCa (lang *SM.Lang) *RenameCa {
	ncLbl := lang.Map("#skillsclient:NewCategoryLbl") + BA.SpL
	phC := lang.Map("#skillsclient:TypeCategory")
	caRen := lang.Map("#skillsclient:CaRen")
	return &RenameCa{ncLbl, phC, caRen}
} //doRenameCa

func printRenCa (title, caPos, spPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(caPos, spPos, lang)
	renameCa := doRenameCa(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, RenameCa: renameCa, Title: t, Error: err}
} //printRenCa

func endRenCa (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == categoryRenameName, name, 100)
	t := "#skillsclient:CategoryRen"
	if r.Method == "GET" {
		out := printRenCa(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	newCategory := r.PostFormValue("newCategory")
	caSp := r.PostFormValue("serList")
	l := strings.Split(caSp, "|")
	oldCategory := l[0]
	speciality := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(oldCategory)
	mk.BuildField("ca")
	mk.PushString(speciality)
	mk.BuildField("sp")
	mk.PushString(newCategory)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, renCaDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	out := printRenCa(t, newCategory, speciality, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 105)
} //endRenCa

func doRenameSp (lang *SM.Lang) *RenameSp {
	nsLbl := lang.Map("#skillsclient:NewSpecialityLbl") + BA.SpL
	phS := lang.Map("#skillsclient:TypeSpeciality")
	spRen := lang.Map("#skillsclient:SpRen")
	return &RenameSp{nsLbl, phS, spRen}
} //doRenameSp

func printRenSp (title, caPos, spPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(caPos, spPos, lang)
	renameSp := doRenameSp(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, RenameSp: renameSp, Title: t, Error: err}
} //printRenSp

func endRenSp (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == specialityRenameName, name, 100)
	t := "#skillsclient:SpecialityRen"
	if r.Method == "GET" {
		out := printRenSp(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	newSpeciality := r.PostFormValue("newSpeciality")
	caSp := r.PostFormValue("serList")
	l := strings.Split(caSp, "|")
	category := l[0]
	oldSpeciality := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(category)
	mk.BuildField("ca")
	mk.PushString(oldSpeciality)
	mk.BuildField("sp")
	mk.PushString(newSpeciality)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, renSpDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	out := printRenSp(t, category, newSpeciality, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 103)
} //endRenSp

func doRemove (lang *SM.Lang) *Remove {
	serRem := lang.Map("#skillsclient:SerRem")
	return &Remove{serRem}
} //doRemove

func printRem (title, caPos, spPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(caPos, spPos, lang)
	remove := doRemove(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, Remove: remove, Title: t, Error: err}
} //printRem

func endRem (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == serviceRemoveName, name, 100)
	t := "#skillsclient:ServiceRem"
	if r.Method == "GET" {
		out := printRem(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	caSp := r.PostFormValue("serList")
	l := strings.Split(caSp, "|")
	category := l[0]
	speciality := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(category)
	mk.BuildField("ca")
	mk.PushString(speciality)
	mk.BuildField("sp")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, remSerDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	ca := ""; sp := ""
	if res.Errors != nil {
		ca = category
		sp = speciality
	}
	out := printRem(t, ca, sp, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 103)
} //endRem

func init() {
	W.RegisterPackage(serviceInsertName, html, endIns, true)
	W.RegisterPackage(categoryRenameName, html, endRenCa, true)
	W.RegisterPackage(specialityRenameName, html, endRenSp, true)
	W.RegisterPackage(serviceRemoveName, html, endRem, true)
} //init
