/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package language

import (
	
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	M	"git.duniter.org/gerard94/util/misc"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"html/template"

)

const (
	
	languageName = "53language"
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				<p>
					<label for="language">{{.Select}}</label>
					<select name="language" id="language" >
						{{range .Languages}}
							<option value="{{.Code}}"{{.Selected}}>{{.Name}}</option>
						{{end}}
					</select>
				</p>
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	stringList []string
	
	// Outputs
	
	lang struct {
		Code,
		Selected,
		Name string
	}
	
	langList []lang
	
	Out struct {
		Title,
		Select,
		OK string
		Languages langList
	}

)

var (
	
	languages = stringList{"en", "fr"}

)

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == languageName, name, 100)
	if r.Method != "GET" {
		r.ParseForm()
		newLang := r.PostFormValue("language")
		data.SetLang(newLang)
		http.Redirect(w, r, "/index", http.StatusFound)
		return
	}
	t := lang.Map("#skillsclient:language")
	sel := lang.Map("#skillsclient:Select") + BA.SpL
	ok := lang.Map("#skillsclient: OK")
	l := make(langList, len(languages))
	language := lang.Language()
	for i, lg := range languages {
		l[i].Code = lg
		if lg == language {
			l[i].Selected = "selected"
		} else {
			l[i].Selected = ""
		}
		l[i].Name = lang.Map("#skillsclient:" + lg)
	}
	out := &Out{t, sel, ok, l}
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
} //end

func init() {
	W.RegisterPackage(languageName, html, end, true)
} //init
