/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package locations

import (
	
	BA	 "git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strings"
		"html/template"
		/*
		"fmt"
		*/

)

const (
	
	locationInsertName = "11locationInsert"
	countryRenameName = "121countryRename"
	regionRenameName = "122regionRename"
	locationRemoveName = "13locationRemove"
	
	queryListLocations = `
		query ListLocations ($co: String) {
			locList: allLocations(country: $co) {
				country
				regions
			}
		}
	`
	
	queryInsertLocation = `
		mutation InsertLocation ($co: String!, $rg: String!) {
			locIns: insertLocation (country: $co, region: $rg) {
				country
				region
			}
		}
	`
	
	queryRenameCountry = `
		mutation RenameCountry ($co: String!, $rg: String, $nN: String!) {
			coRen: renameLocation(location: COUNTRY, country: $co, region: $rg, newName: $nN)
		}
	`
	
	queryRenameRegion = `
		mutation RenameRegion ($co: String!, $rg: String!, $nN: String!) {
			rgRen: renameLocation(location: REGION, country: $co, region: $rg, newName: $nN)
		}
	`
	
	queryRemoveLocation = `
		mutation RemoveLocation ($co: String!, $rg: String!) {
			locRem: removeLocation(country: $co, region: $rg)
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .List}}
					<p>
						<label for="locList">{{.LocListLbl}}</label>
						{{$coPos := .CoPos}}
						{{$rgPos := .RgPos}}
						<select name="locList" id="locList" >
							{{range $co := .Locations}}
								{{range $rg := $co.Regions}}
									<option value="{{$co.Country}}|{{$rg}}" {{if and (eq $co.Country $coPos) (eq $rg $rgPos)}}selected{{end}}>{{$co.Country}} / {{$rg}}</option>
								{{end}}
							{{end}}
						</select>
					</p>
				{{end}}
				{{with .Insert}}
					<p>
						<label for="countryId">{{.CountryLbl}}</label>
						<input type="text" name="country" id="countryId" placeholder="{{.PlaceholderC}}" value="{{.Country}}" size = 35/>
					</p>
					<p>
						<label for="regionId">{{.RegionLbl}}</label>
						<input type="text" name="region" id="regionId" placeholder="{{.PlaceholderR}}" value="{{.Region}}" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.AddLoc}}">
					</p>
				{{end}}
				{{with .RenameCo}}
					<p>
						<label for="newCountryId">{{.NewCoNameLbl}}</label>
						<input type="text" name="newCountry" id="newCountryId" placeholder="{{.PlaceholderNC}}" value="" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.CoRen}}">
					</p>
				{{end}}
				{{with .RenameRg}}
					<p>
						<label for="newRegionId">{{.NewRgNameLbl}}</label>
						<input type="text" name="newRegion" id="newRegionId" placeholder="{{.PlaceholderNR}}" value="" size = 35/>
					</p>
					<p>
						<input type="submit" value="{{.RgRen}}">
					</p>
				{{end}}
				{{with .Remove}}
					<p>
						<input type="submit" value="{{.LocRem}}">
					</p>
				{{end}}
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Location struct {
		Country string
		Regions []string
	}
	
	Location1 struct {
		Country,
		Region string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			LocList []*Location
			LocIns *Location1
			CoRen,
			RgRen,
			LocRem bool
		}
	}
	
	// Outputs
	
	List struct {
		LocListLbl,
		CoPos,
		RgPos string
		Locations []*Location
	}
	
	Insert struct {
		CountryLbl,
		PlaceholderC,
		Country,
		RegionLbl,
		PlaceholderR,
		Region,
		AddLoc string
	}
	
	RenameCo struct {
		NewCoNameLbl,
		PlaceholderNC,
		CoRen string
	}
	
	RenameRg struct {
		NewRgNameLbl,
		PlaceholderNR,
		RgRen string
	}
	
	Remove struct {
		LocRem string
	}
	
	Out struct {
		List *List
		Insert *Insert
		RenameCo *RenameCo
		RenameRg *RenameRg
		Remove *Remove
		Title,
		Error string
	}

)

var (
	
	listLocDoc = GS.ExtractDocument(queryListLocations)
	insLocDoc = GS.ExtractDocument(queryInsertLocation)
	coRenDoc = GS.ExtractDocument(queryRenameCountry)
	rgRenDoc = GS.ExtractDocument(queryRenameRegion)
	locRemDoc = GS.ExtractDocument(queryRemoveLocation)

)

func doList (coPos, rgPos string, lang *SM.Lang) *List {
	lLbl := lang.Map("#skillsclient:LocList") + BA.SpL
	j := GS.Send(nil, listLocDoc, "")
	if _, b := J.GetValue(j.(*J.Object), "errors"); b {
		M.Halt(j.GetString(), 100)
	}
	res := new(Res)
	J.ApplyTo(j, res)
	locs := res.Data.LocList
	return &List{lLbl, coPos, rgPos, locs}
} //doList

func doInsert (co, rg string, lang *SM.Lang) *Insert {
	cLbl := lang.Map("#skillsclient:CountryLbl") + BA.SpL
	phC := lang.Map("#skillsclient:TypeCountry")
	rLbl := lang.Map("#skillsclient:RegionLbl") + BA.SpL
	phR := lang.Map("#skillsclient:TypeRegion")
	addLoc := lang.Map("#skillsclient:AddLoc")
	return &Insert{cLbl, phC, co, rLbl, phR, rg, addLoc}
} //doInsert

func printIns (title, coPos, co, rgPos, rg string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(coPos, rgPos, lang)
	insert := doInsert(co, rg, lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, Insert: insert, Title: t, Error: err}
} //printIns

func endIns (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == locationInsertName, name, 100)
	t := "#skillsclient:LocationsIns"
	if r.Method == "GET" {
		out := printIns(t, "", "", "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	country := r.PostFormValue("country")
	region := r.PostFormValue("region")
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(country)
	mk.BuildField("co")
	mk.PushString(region)
	mk.BuildField("rg")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, insLocDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	co := ""; rg := ""
	if res.Errors != nil {
		co = country
		rg = region
	}
	out := printIns(t, country, co, region, rg, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //endIns

func doRenameCo (lang *SM.Lang) *RenameCo {
	ncLbl := lang.Map("#skillsclient:NewCountryLbl") + BA.SpL
	phC := lang.Map("#skillsclient:TypeCountry")
	coRen := lang.Map("#skillsclient:CoRen")
	return &RenameCo{ncLbl, phC, coRen}
} //doRenameCo

func printCoRen (title, coPos, rgPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(coPos, rgPos, lang)
	renameCo := doRenameCo(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, RenameCo: renameCo, Title: t, Error: err}
} //printCoRen

func endCoRen (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == countryRenameName, name, 100)
	t := "#skillsclient:CountryRen"
	if r.Method == "GET" {
		out := printCoRen(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	newCountry := r.PostFormValue("newCountry")
	coRg := r.PostFormValue("locList")
	l := strings.Split(coRg, "|")
	oldCountry := l[0]
	region := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(oldCountry)
	mk.BuildField("co")
	mk.PushString(region)
	mk.BuildField("rg")
	mk.PushString(newCountry)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, coRenDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	out := printCoRen(t, newCountry, region, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //endCoRen

func doRenameRg (lang *SM.Lang) *RenameRg {
	nrLbl := lang.Map("#skillsclient:NewRegionLbl") + BA.SpL
	phR := lang.Map("#skillsclient:TypeRegion")
	rgRen := lang.Map("#skillsclient:RgRen")
	return &RenameRg{nrLbl, phR, rgRen}
} //doRenameRg

func printRgRen (title, coPos, rgPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(coPos, rgPos, lang)
	renameRg := doRenameRg(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, RenameRg: renameRg, Title: t, Error: err}
} //printRgRen

func endRgRen (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == regionRenameName, name, 100)
	t := "#skillsclient:RegionRen"
	if r.Method == "GET" {
		out := printRgRen(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	newRegion := r.PostFormValue("newRegion")
	coRg := r.PostFormValue("locList")
	l := strings.Split(coRg, "|")
	country := l[0]
	oldRegion := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(country)
	mk.BuildField("co")
	mk.PushString(oldRegion)
	mk.BuildField("rg")
	mk.PushString(newRegion)
	mk.BuildField("nN")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, rgRenDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	out := printRgRen(t, country, newRegion, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //endRgRen

func doRemove (lang *SM.Lang) *Remove {
	locRem := lang.Map("#skillsclient:LocRem")
	return &Remove{locRem}
} //doRemove

func printRem (title, coPos, rgPos string, errs []BA.Error, lang *SM.Lang) *Out {
	t := lang.Map(title)
	list := doList(coPos, rgPos, lang)
	remove := doRemove(lang)
	err := BA.DoError(errs, lang)
	return &Out{List: list, Remove: remove, Title: t, Error: err}
} //printRem

func endRem (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == locationRemoveName, name, 100)
	t := "#skillsclient:LocationRem"
	if r.Method == "GET" {
		out := printRem(t, "", "", nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	coRg := r.PostFormValue("locList")
	l := strings.Split(coRg, "|")
	country := l[0]
	region := l[1]
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(country)
	mk.BuildField("co")
	mk.PushString(region)
	mk.BuildField("rg")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, locRemDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	co := ""; rg := ""
	if res.Errors != nil {
		co = country
		rg = region
	}
	out := printRem(t, co, rg, res.Errors, lang)
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
} //endRem

func init() {
	W.RegisterPackage(locationInsertName, html, endIns, true)
	W.RegisterPackage(countryRenameName, html, endCoRen, true)
	W.RegisterPackage(regionRenameName, html, endRgRen, true)
	W.RegisterPackage(locationRemoveName, html, endRem, true)
} //init
