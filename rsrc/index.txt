Manager: Insert Locations	11locationInsert

Manager: Rename Countries	121countryRename

Manager: Rename Regions	122regionRename

Manager: Remove Locations	13locationRemove

Manager: Insert Services	21serviceInsert

Manager: Rename Categories	221categoryRename

Manager: Rename Specialities	222specialityRename

Manager: Remove Services	23serviceRemove

Manager: Create Credentials	311credentialsCreate

Manager: Reset Credentials	312credentialsReset

Manager: Change Identity	32identityChange

Manager: Remove Identity	33identityRemove

Manager: Lock System	34lockSystem

Search Services	41identitiesSearch

Scan Identities	42identitiesScan

Change Identity	43identityChange

Change Password	44passwordChange

Change User	51signIn

SignOut	52signOut

Choose Language	53language
