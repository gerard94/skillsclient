/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package identityRemove

import (
	
	BA	 "git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"html/template"
		/*
		"fmt"
		*/

)

const (
	
	identitiesRemoveName = "33identityRemove"
	
	queryListIdentities = `
		query ListIdentities ($h: String) {
			idList: allIdentities(hint: $h) {
				nickname
			}
		}
	`
	
	queryRemoveIdentity = `
		mutation RemoveIdentity ($nN: String!) {
			id: removeIdentity(nickname: $nN) {
				nickname
			}
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .Start}}
					<p>
						<label for="hintId">{{.HintLbl}}</label>
						<input type="text" name="hint" id="hintId" placeholder="{{.HintPH}}" value="{{.Hint}}" size = 35/>
					</p>
					<input type="hidden" name="oldHint" value="{{.Hint}}"/>
				{{end}}
				{{with .Find}}
					<p>
						<label for="idListId">{{.IdListLbl}}</label>
						{{$selN := .SelectedNn}}
						<select name="idList" id="idListId" >
							{{range $nick:= .Nicks}}
								<option value="{{$nick}}" {{if eq $nick $selN}}selected{{end}}>{{$nick}}</option>
							{{end}}
						</select>
					</p>
					<input type="hidden" name="oldSelN" value="{{.SelectedNn}}"/>
				{{end}}
				{{with .Fix}}
					{{.Notice}}
				{{end}}
				{{if .Message}}
					<p>
					 {{.Message}}
					<input type="hidden" name="message" value="{{.NewHint}}"/>
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Identity struct {
		Nickname string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			IdList []*Identity
			Id *Identity
		}
	}
	
	// Outputs
	
	Start struct {
		HintLbl,
		HintPH,
		Hint string
	}
	
	Find struct {
		IdListLbl,
		SelectedNn string
		Nicks []string
	}
	
	Fix struct {
		Notice string
	}
	
	Out struct {
		Message,
		NewHint,
		Title,
		OK string
		Start *Start
		Find *Find
		Fix	*Fix
	}

)

var (
	
	idListDoc = GS.ExtractDocument(queryListIdentities)
	idRemDoc = GS.ExtractDocument(queryRemoveIdentity)

)

func doBase (t string, lang *SM.Lang) (title, ok string) {
	title = lang.Map(t)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doStart (hint string, lang *SM.Lang) *Start {
	hl := lang.Map("#skillsclient:HintLbl") + BA.SpL
	hp := lang.Map("#skillsclient:HintPlaceHolder")
	return &Start{hl, hp, hint}
}

func printStart (title, hint string, lang *SM.Lang) *Out {
	t, ok := doBase(title, lang)
	start := doStart(hint, lang)
	return &Out{Title: t, OK: ok, Start: start}
}

func doFind (selNN string, idList []*Identity, lang *SM.Lang) *Find {
	idlLbl := lang.Map("#skillsclient:IdListLbl") + BA.SpL
	nicks := make([]string, len(idList))
	for i, id := range idList {
		nicks[i] = id.Nickname
	}
	return &Find{idlLbl, selNN, nicks}
}

func printFind (t, hint, selNN string, idList []*Identity, lang *SM.Lang) *Out {
	t, ok := doBase(t, lang)
	start := doStart(hint, lang)
	find := doFind(selNN, idList, lang)
	return &Out{Title: t, OK: ok, Start: start, Find: find}
}

func doFix (nickName string, done bool, lang *SM.Lang) *Fix {
	var note string
	if done {
		note = lang.Map("#skillsclient:RemDone", nickName)
	} else {
		note = lang.Map("#skillsclient:RemNotice", nickName)
	}
	return &Fix{note}
}

func printFix (t, hint, selNN string, idList []*Identity, nickName string, done bool, lang *SM.Lang) *Out {
	t, ok := doBase(t, lang)
	start := doStart(hint, lang)
	find := doFind(selNN, idList, lang)
	fix := doFix(nickName, done, lang)
	return &Out{Title: t, OK: ok, Start: start, Find: find, Fix: fix}
} //print

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	if !data.IsAdmin() {
		http.Redirect(w, r, "/index", http.StatusFound)
	}
	lang := data.Lang()
	M.Assert(name == identitiesRemoveName, name, 100)
	t := "#skillsclient:IdentityRemove"
	if r.Method == "GET" {
		out := printStart(t, "", lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 101)
		return
	}
	r.ParseForm()
	mes := r.PostFormValue("message")
	if mes != "" {
		out := printStart(t, mes, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 102)
		return
	}
	hint := r.PostFormValue("hint")
	oldHint := r.PostFormValue("oldHint")
	selNN := r.PostFormValue("idList")
	oldSelN := r.PostFormValue("oldSelN")
	isFix := hint == oldHint && selNN != ""
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(hint)
	mk.BuildField("h")
	mk.BuildObject()
	j := mk.GetJson()
	j = GS.Send(j, idListDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	M.Assert(res.Errors == nil, 102)
	if !isFix {
		M.Assert(res.Data.IdList != nil, 103)
		if len(res.Data.IdList) != 1 {
			out := printFind(t, hint, selNN, res.Data.IdList, lang)
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 104)
			return
		}
		selNN = res.Data.IdList[0].Nickname
	}
	
	if selNN != oldSelN {
		out := printFix(t, hint, selNN, res.Data.IdList, selNN, false, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 105)
		return
	}
	
	mk = J.NewMaker()
	mk.StartObject()
	mk.PushString(selNN)
	mk.BuildField("nN")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, idRemDoc, "")
	J.ApplyTo(j, res)
	M.Assert(res.Errors == nil, BA.DoError(res.Errors, lang), 106)
	out := &Out{Message: lang.Map("#skillsclient:RemDone", selNN), NewHint: hint, Title: lang.Map(t), OK: lang.Map("#skillsclient:OK")}
	err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 107)
} //end

func init() {
	W.RegisterPackage(identitiesRemoveName, html, end, true)
} //init
