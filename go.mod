module git.duniter.org/gerard94/skillsclient

go 1.20

require (
	git.duniter.org/gerard94/skillsKeys v0.0.0-00010101000000-000000000000
	git.duniter.org/gerard94/util v1.0.3
)

replace git.duniter.org/gerard94/util => ../../util

replace git.duniter.org/gerard94/skillsKeys => ../skillsKeys
