/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package identitiesSearch

import (
	
	A	"git.duniter.org/gerard94/util/avlT"
	BA	"git.duniter.org/gerard94/skillsclient/basic"
	GS	"git.duniter.org/gerard94/skillsclient/gqSender"
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	SM	"git.duniter.org/gerard94/util/strMapping"
	ST	"git.duniter.org/gerard94/util/strings"
	W	"git.duniter.org/gerard94/skillsclient/web"
		"net/http"
		"strconv"
		"strings"
		"html/template"
		
		/*
		"fmt"
		*/

)

const (
	
	identitiesSearchName = "41identitiesSearch"
	
	queryListLocations = `
		query ListLocations ($co: String, $rg: String) {
			locList: locationList(country: $co, region: $rg) {
				country
				region
				city
			}
		}
	`
	
	queryListServices = `
		query ListServices ($co: String, $rg: String, $cy: String, $ca: String, $sesu: SearchedOrSupplied) {
			serList: serviceList(country: $co, region: $rg, city: $cy, category: $ca, searchedOrSupplied: $sesu) {
				category
				speciality
			}
		}
	`
	
	queryIdentityList = `
		query IdentityList ($co: String, $rg: String, $cy: String, $ser: [ServiceQ_input!], $sesu: SearchedOrSupplied) {
			idList: identityList(country: $co, region: $rg, city: $cy, services: $ser, searchedOrSupplied: $sesu) {
				nickname
			}
		}
	`
	
	queryFindIdentity = `
		query FindIdentity ($nN: String!) {
			id: identityOf(nickname: $nN) {
				nickname
				g1_nickname
				pubkey
				email
				locations {
					country
					regions {
						region
						cities
					}
				}
				transport
				pickup
				supplied_services {
					...Services
				}
				searched_services {
					...Services
				}
				additional_informations
			}
		}
		fragment Services on Service {
			category
			specialities
		}
	`
	
	html = `
		{{define "head"}}<title>{{.Title}}</title>{{end}}
		{{define "body"}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
			<h1>{{.Title}}</h1>
			<form action="" method="post">
				{{with .LocList}}
					<p>
						<label for="locList">{{.LocListLbl}}</label>
						<br>
						{{$locPos := .LocPos}}
						<select name="locList" id="locList" >
							{{range $l := .Locations}}
								<option value="{{$l.Country}}|{{$l.Region}}{{if $l.City}}|{{$l.City}}{{end}}" {{if and (eq $l.Country $locPos.Country) (eq $l.Region $locPos.Region) (eq $l.City $locPos.City)}}selected{{end}}>{{$l.Country}} / {{$l.Region}}{{if $l.City}} / {{$l.City}}{{end}}</option>
							{{end}}
						</select>
					</p>
					<input type="hidden" name="locPosH" value="{{.LocPosH}}"/>
				{{end}}
				{{with .Sesu}}
					<p>
						{{.SesuLbl}}
						<br>
						<input type="radio" name="sesu" id="suId" value="0"{{if eq .Sesu 0}}checked{{end}}/><label for="suId">{{.SuLbl}}</label>
						<br>
						<input type="radio" name="sesu" id="seId" value="1" {{if eq .Sesu 1}}checked{{end}}/><label for="seId">{{.SeLbl}}</label>
						<br>
						<input type="radio" name="sesu" id="bothId" value="2" {{if eq .Sesu 2}}checked{{end}}/><label for="bothId">{{.BothLbl}}</label>
					</p>
					<input type="hidden" name="sesuH" value="{{.SesuH}}"/>
				{{end}}
				{{with .SerList}}
					<p>
						<label for="serList">{{.SerListLbl}}</label>
						<br>
						{{$serPos := .SerPos}}
						<select name="serList" id="serList" multiple>
							{{range $s := .Services}}
								<option value="{{$s.Category}}|{{$s.Speciality}}" {{range $p := $serPos}}{{if and (eq $s.Category $p.Category) (eq $s.Speciality $p.Speciality)}}selected{{end}}{{end}}>{{$s.Category}} / {{$s.Speciality}}</option>
							{{end}}
						</select>
					</p>
					<input type="hidden" name="SerPosH" value="{{.SerPosH}}"/>
				{{end}}
				{{with .Find}}
					<p>
						<label for="idListId">{{.IdListLbl}}</label>
						{{$selN := .SelectedNn}}
						<select name="idList" id="idListId" >
							{{range $nick:= .Nicks}}
								<option value="{{$nick}}" {{if eq $nick $selN}}selected{{end}}>{{$nick}}</option>
							{{end}}
						</select>
					</p>
				{{end}}
				<p>
					<input type="submit" value="{{.OK}}">
				</p>
			</form>
			{{if .Error}}
				<p>
					{{.Error}}
				</p>
			{{end}}
			{{with .Fix}}
				<p>
					{{.Nickname}}
				</p>
				<p>
					{{.G1_nickname}}
				</p>
				<p>
					{{.Pubkey}}
				</p>
				<p>
					{{.Email}}
				</p>
				<p>
					{{.Locations}}
					<blockquote>
					{{range $co := .Locs}}
						{{range $rg := $co.Regions}}
							{{if eq (len $rg.Cities) 0}}
								{{$co.Country}} / {{$rg.Region}}
								<br>
							{{else}}
								{{range $cy := $rg.Cities}}
									{{$co.Country}} / {{$rg.Region}} / {{$cy}}
									<br>
								{{end}}
							{{end}}
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Transport}}
				</p>
				<p>
					{{.Pickup}}
				</p>
				<p>
					{{.Supplied_services}}
					<blockquote>
					{{range $ca := .Su_ser}}
						{{range $sp := $ca.Specialities}}
							{{$ca.Category}} / {{$sp}}
							<br>
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Searched_services}}
					<blockquote>
					{{range $ca := .Se_ser}}
						{{range $sp := $ca.Specialities}}
							{{$ca.Category}} / {{$sp}}
							<br>
						{{end}}
					{{end}}
					</blockquote>
				</p>
				<p>
					{{.Additional_informations}}
				</p>
			{{end}}
			<p>
				<a href = "/index">{{Map "index"}}</a>
			</p>
		{{end}}
	`

)

type (
	
	Location1 struct {
		Country,
		Region,
		City string
	}
	
	Region struct {
		Region string
		Cities []string
	}
	
	Location struct {
		Country string
		Regions []Region
	}
	
	Service1 struct {
		Category,
		Speciality string
	}
	
	Service struct {
		Category string
		Specialities []string
	}
	
	Identity struct {
		Nickname,
		G1_nickname,
		Pubkey,
		Email string
		Locations []Location
		Transport,
		Pickup bool
		Supplied_services,
		Searched_services []Service
		Additional_informations string
	}
	
	Res struct {
		Errors []BA.Error
		Data struct {
			LocList []Location1
			SerList []Service1
			IdList []Identity
			Id *Identity
		}
	}
	
	// Outputs
	
	LocList struct {
		LocListLbl,
		LocPosH string
		LocPos *Location1
		Locations []Location1
	}
	
	Sesu struct {
		SesuLbl,
		SuLbl,
		SeLbl,
		BothLbl,
		SesuH string
		Sesu int
	}
	
	SerList struct {
		SerListLbl,
		SerPosH string
		SerPos,
		Services []Service1
	}
	
	Find struct {
		IdListLbl,
		SelectedNn string
		Nicks []string
	}
	
	Fix struct {
		Nickname,
		G1_nickname,
		Pubkey,
		Email,
		Locations,
		Transport,
		Pickup,
		Supplied_services,
		Searched_services,
		Additional_informations string
		Locs []Location
		Su_ser,
		Se_ser []Service
	}
	
	Out struct {
		LocList *LocList
		Sesu *Sesu
		SerList *SerList
		Find *Find
		Fix *Fix
		Title,
		Error,
		OK string
	}

)

var (
	
	listLocDoc = GS.ExtractDocument(queryListLocations)
	listSerDoc = GS.ExtractDocument(queryListServices)
	listIdDoc = GS.ExtractDocument(queryIdentityList)
	findIdDoc = GS.ExtractDocument(queryFindIdentity)

)

var (
	
	searchSupply = [...]string{"SUPPLIED_SERVICES", "SEARCHED_SERVICES", "BOTH_SERVICES"}

)

func (ser1 *Service) Compare (ser2 *Service) ST.Comp {
	return ST.CompP(ser1.Category, ser2.Category)
}

func splitLocPos (locPosS string) *Location1 {
	l := strings.Split(locPosS, "|")
	n := len(l)
	if n == 0 {
		return nil
	}
	M.Assert(n == 2 || n == 3, n, 20)
	coPos := l[0]
	rgPos := l[1]
	cyPos := ""
	if n == 3 {
		cyPos = l[2]
	}
	return &Location1{coPos, rgPos, cyPos}
}

func splitSerPos (serPosL []string) []Service1 {
	M.Assert(serPosL != nil, 20)
	serPos := make([]Service1, len(serPosL))
	for i, ser := range serPosL {
		l := strings.Split(ser, "|")
		n := len(l)
		M.Assert(n == 2, n, 20)
		serPos[i] = Service1{l[0], l[1]}
	}
	return serPos
}

func serPosLToString (serPosL []string) string {
	for i := 1; i < len(serPosL); i++ {
		for j := i; j > 0 && ST.CompP(serPosL[j], serPosL[j - 1]) == ST.Lt; j-- {
			serPosL[j], serPosL[j - 1] = serPosL[j - 1], serPosL[j]
		}
	}
	var b strings.Builder
	for i, pos := range serPosL {
		if i > 0 {
			b.WriteString("|")
		}
		b.WriteString(pos)
	}
	return b.String()
}

func ser1ToSer (s1 []Service1) *A.Tree[*Service] {
	l := A.New[*Service]()
	for _, ss := range s1 {
		e, _, _ := A.SearchIns[*Service](l, &Service{ss.Category, make([]string, 0)})
		s := e.Val()
		s.Specialities = append(s.Specialities, ss.Speciality)
	}
	for e := l.Next(nil); e != nil; e = l.Next(e) {
		s := e.Val()
		sps := s.Specialities
		for i := 1; i < len(sps); i++ {
			for j := i; j > 0 && ST.CompP(sps[j], sps[j - 1]) == ST.Lt; j-- {
				sps[j], sps[j - 1] = sps[j - 1], sps[j]
			}
		}
		s.Specialities = sps
		e.SetVal(s)
	}
	return l
}

func doBase (title string, errs []BA.Error, lang *SM.Lang) (t, err, ok string) {
	t = lang.Map(title)
	err = BA.DoError(errs, lang)
	ok = lang.Map("#skillsclient:OK")
	return
}

func doLoc (locPosS string, locPos *Location1, locs []Location1, lang *SM.Lang) *LocList {
	lLbl := lang.Map("#skillsclient:LocList")
	if locPos == nil {
		locPos = &Location1{"", "", ""}
	}
	return &LocList{lLbl, locPosS, locPos, locs}
}

func doSesu (sesuS string, sesuI int, lang *SM.Lang) *Sesu {
	sLbl := lang.Map("#skillsclient:SesuLbl")
	suLbl := lang.Map("#skillsclient:SuLbl")
	seLbl := lang.Map("#skillsclient:SeLbl")
	bLbl := lang.Map("#skillsclient:BothLbl")
	return &Sesu{sLbl, suLbl, seLbl, bLbl, sesuS, sesuI}
}

func printLoc (title, locPosS string, locPos *Location1, locs []Location1, sesuS string, sesuI int, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	llist := doLoc(locPosS, locPos, locs, lang)
	sesu := doSesu(sesuS, sesuI, lang)
	return &Out{LocList: llist, Sesu: sesu, Title: t, Error: err, OK: ok}
}

func doSer (serPosS string, serPos, sers []Service1, lang *SM.Lang) *SerList {
	sLbl := lang.Map("#skillsclient:SerList")
	return &SerList{sLbl, serPosS, serPos, sers}
}

func printSer (title, locPosS string, locPos *Location1, locs []Location1, sesuS string, sesuI int, serPosS string, serPos, sers []Service1, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	llist := doLoc(locPosS, locPos, locs, lang)
	sesu := doSesu(sesuS, sesuI, lang)
	slist := doSer(serPosS, serPos, sers, lang)
	return &Out{LocList: llist, Sesu: sesu, SerList: slist, Title: t, Error: err, OK: ok}
}

func doFind (selNN string, idList []Identity, lang *SM.Lang) *Find {
	if len(idList) == 0 {
		return nil
	}
	idlLbl := lang.Map("#skillsclient:IdListLbl") + BA.SpL
	nicks := make([]string, len(idList))
	for i, id := range idList {
		nicks[i] = id.Nickname
	}
	return &Find{idlLbl, selNN, nicks}
}

func printFind (title, locPosS string, locPos *Location1, locs []Location1, sesuS string, sesuI int, serPosS string, serPos, sers []Service1, selNN string, idList []Identity, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	llist := doLoc(locPosS, locPos, locs, lang)
	sesu := doSesu(sesuS, sesuI, lang)
	slist := doSer(serPosS, serPos, sers, lang)
	find := doFind(selNN, idList, lang)
	return &Out{LocList: llist, Sesu: sesu, SerList: slist, Find: find, Title: t, Error: err, OK: ok}
}

func doFix (id *Identity, lang *SM.Lang) *Fix {
	
	yesNo := func (b bool) string {
		if b {
			return lang.Map("#skillsclient:Yes")
		}
		return lang.Map("#skillsclient:No")
	}
	
	if id == nil {
		return nil
	}
	nN := lang.Map("#skillsclient:NicknameL", "") + BA.SpL + id.Nickname
	gNN := lang.Map("#skillsclient:G1_nickname") + BA.SpL + id.G1_nickname
	pub := lang.Map("#skillsclient:Pubkey", "") + BA.SpL + id.Pubkey
	mail := lang.Map("#skillsclient:Email") + BA.SpL + id.Email
	locations := lang.Map("#skillsclient:Locations", "")
	transport := lang.Map("#skillsclient:Transport") + BA.SpL + yesNo(id.Transport)
	pickup := lang.Map("#skillsclient:Pickup") + BA.SpL + yesNo(id.Pickup)
	sup := lang.Map("#skillsclient:Supplied_services", "")
	sea := lang.Map("#skillsclient:Searched_services")
	add := lang.Map("#skillsclient:Add_Info") + BA.SpL + id.Additional_informations
	return &Fix{nN, gNN, pub, mail, locations, transport, pickup, sup, sea, add, id.Locations, id.Supplied_services, id.Searched_services}
}

func printFix (title, locPosS string, locPos *Location1, locs []Location1, sesuS string, sesuI int, serPosS string, serPos, sers []Service1, selNN string, idList []Identity, id *Identity, errs []BA.Error, lang *SM.Lang) *Out {
	t, err, ok := doBase(title, errs, lang)
	llist := doLoc(locPosS, locPos, locs, lang)
	sesu := doSesu(sesuS, sesuI, lang)
	slist := doSer(serPosS, serPos, sers, lang)
	find := doFind(selNN, idList, lang)
	fix := doFix(id, lang)
	return &Out{LocList: llist, Sesu: sesu, SerList: slist, Find: find, Fix: fix, Title: t, Error: err, OK: ok}
} //print

func end (name string, temp *template.Template, r *http.Request, w http.ResponseWriter, data W.SharedData) {
	lang := data.Lang()
	M.Assert(name == identitiesSearchName, name, 20)
	t := "#skillsclient:IdentitiesSearch"
	
	j := GS.Send(nil, listLocDoc, "")
	resLoc := new(Res)
	J.ApplyTo(j, resLoc)
	M.Assert(resLoc.Errors == nil, 100)
	
	if r.Method == "GET" {
		out := printLoc(t, "", nil, resLoc.Data.LocList, "0", 0, nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 100)
		return
	}
	
	r.ParseForm()
	locPosS := r.PostFormValue("locList")
	locPos := splitLocPos(locPosS)
	locPosH := r.PostFormValue("locPosH")
	sesuS := r.PostFormValue("sesu")
	sesuI, err := strconv.Atoi(sesuS); M.Assert(err == nil, err, 101)
	sesuH := r.PostFormValue("sesuH")
	sesu := searchSupply[sesuI]
	
	if locPos == nil {
		out := printLoc(t, locPosS, locPos, resLoc.Data.LocList, sesuS, sesuI, nil, lang)
		err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 100)
		return
	}
	
	serPosL, ok := r.PostForm["serList"]
	if !ok || serPosL == nil {
		serPosL = make([]string, 0)
	}
	serPosS := serPosLToString(serPosL)
	serPos := splitSerPos(serPosL)
	serPosH := r.PostFormValue("SerPosH")
	
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(locPos.Country)
	mk.BuildField("co")
	mk.PushString(locPos.Region)
	mk.BuildField("rg")
	mk.PushString(locPos.City)
	mk.BuildField("cy")
	mk.PushString(sesu)
	mk.BuildField("sesu")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, listSerDoc, "")
	resSer := new(Res)
	J.ApplyTo(j, resSer)
	M.Assert(resSer.Errors == nil, 102)
		
	if len(serPos) == 0 || locPosS != locPosH || sesuS != sesuH {
		out := printSer(t, locPosS, locPos, resLoc.Data.LocList, sesuS, sesuI, serPosS, serPos, resSer.Data.SerList, nil, lang)
		err = temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 103)
		return
	}
	
	selNN := r.PostFormValue("idList")
	isFix := locPosS == locPosH && sesuS == sesuH && serPosS == serPosH && selNN != ""
	
	serQ := ser1ToSer(serPos)
	
	mk = J.NewMaker()
	mk.StartObject()
	mk.PushString(locPos.Country)
	mk.BuildField("co")
	mk.PushString(locPos.Region)
	mk.BuildField("rg")
	mk.PushString(locPos.City)
	mk.BuildField("cy")
	mk.PushString(sesu)
	mk.BuildField("sesu")
	mk.StartArray()
	for e := serQ.Next(nil); e != nil; e = serQ.Next(e) {
		ser := e.Val()
		mk.StartObject()
		mk.PushString(ser.Category)
		mk.BuildField("category")
		mk.StartArray()
		for _, sp := range ser.Specialities {
			mk.PushString(sp)
		}
		mk.BuildArray()
		mk.BuildField("specialities")
		mk.BuildObject()
	}
	mk.BuildArray()
	mk.BuildField("ser")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, listIdDoc, "")
	res := new(Res)
	J.ApplyTo(j, res)
	M.Assert(res.Errors == nil, 102)
	if !isFix {
		M.Assert(res.Data.IdList != nil, 103)
		if len(res.Data.IdList) != 1 {
			out := printFind(t, locPosS, locPos, resLoc.Data.LocList, sesuS, sesuI, serPosS, serPos, resSer.Data.SerList, selNN, res.Data.IdList, nil, lang)
			err := temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 104)
			return
		}
		selNN = res.Data.IdList[0].Nickname
	}
	mk = J.NewMaker()
	mk.StartObject()
	mk.PushString(selNN)
	mk.BuildField("nN")
	mk.BuildObject()
	j = mk.GetJson()
	j = GS.Send(j, findIdDoc, "")
	J.ApplyTo(j, res)
	out := printFix(t, locPosS, locPos, resLoc.Data.LocList, sesuS, sesuI, serPosS, serPos, resSer.Data.SerList, selNN, res.Data.IdList, res.Data.Id, res.Errors, lang)
	err = temp.ExecuteTemplate(w, name, out); M.Assert(err == nil, err, 105)
} //end

func init() {
	W.RegisterPackage(identitiesSearchName, html, end, true)
} //init
