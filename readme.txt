Skills est une suite logicielle en ligne ayant pour but de recenser les compétences des utilisateurs de la june afin de faciliter le développement d'une économie en monnaie libre.

Cette suite comprend les exécutables runSkills (git.duniter.org/gerard94/skills/-/releases), runSkillsC (git.duniter.org/gerard94/skillsclient/-/releases), createMasterKey and createManagerKey (git.duniter.org/gerard94/skillskeys/-/releases). Elle permet à chaque participant de renseigner une fiche sur ses compétences et de rechercher, parmi toutes les fiches, les compétences qui l'intéressent.

Pour l'utiliser, vous devez vous rapprocher d'un administrateur de Skills qui vous donnera l'adresse du site et un mot de passe en échange de votre pseudo.
